# BNA-BBOT sounds db

Building a new software architecture for the sounds database of http://www.bna-bbot.be, using Collective Access.


## IMPORT DATA ROUTINE
1. For each sound_arts entry from ODB, create corresponding CA recording object and/or CA album collection => get correspondance table between ids from ODB and ids from ca_objects and ids from ca_collection.
2. Rename each mp3 file to corresponding idno from CA and copy it to import/sounds folder of CA 
3. For each contact entry from ODB, create corresponding CA individual entity. => get correspondance table between ids from ODB and ids from ca_entities.
4. Update objects relations with collections and entities (using correspondance tables).
