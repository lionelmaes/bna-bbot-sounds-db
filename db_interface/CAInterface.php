<?php
require('inc/functions.inc');
require('cawrapper/loader.php');


class CAController
{
  //url has to be as : 'http://dev.bna-bbot.be/ctrl'
  private $CAurl = null;
  private $CAuser = null;
  private $CAkey = null;
  private $collectionModel = null;
  private $objectModel = null;
  private $entityModel = null;
  private $objectEntityRelationNames = null;

  public function __construct($url, $user, $key, $locale, $page)
  {
    $this->CAurl = $url;
    $this->CAuser = $user;
    $this->CAkey = $key;
    $this->locale = $locale;
    $this->objectModel = $this->getObjectModel();
    $this->entityModel = $this->getEntityModel();
    $this->objectEntityRelationNames = $this->getRecordingEntityRelationNames();
    $this->page = $page;
    setlocale(LC_TIME, $this->locale);
  }

  # Collective Access Web API tools

  /**
   * Mapping of the different languages of a field in a cleaner array structure
   */
  private function mapAllLocales($items)
  {
    $mapped = array();
    if ($items != null) {

      foreach ($items as $item) {
        foreach ($item as $key => $item_lang) {
          foreach ($item_lang as $sub) {
            foreach ($sub as $value) {
              if ($key == 1) $mapped['EN'] = $value;
              if ($key == 2) $mapped['FR'] = $value;
              if ($key == 3) $mapped['NL'] = $value;
            }
          }
        }
      }
    } else return '';
    return $mapped;
  }

  //used for itemService ca_objects especially
  private function mapAllLocales2($items)
  {
    $mapped = array();
    if ($items != null) {

      foreach ($items as $key => $item) {
        foreach ($item as $item_lang) {
          foreach ($item_lang as $value) {
            // echo($value);

              if ($key == 'en_US') $mapped['EN'] = $value;
              if ($key == 'fr_BE') $mapped['FR'] = $value;
              if ($key == 'nl_BE') $mapped['NL'] = $value;
          
          }
        }
      }
    } else return '';
    return $mapped;
  }

  // CORE QUERY FUNCTIONS

  //trick to authenticate with CA
  public function getObjectModel()
  {
    if (($this->objectModel) == null)
      return $this->getModel('ca_objects');
    else return $this->objectModel;
  }

  public function getEntityModel()
  {
    if (($this->entityModel) == null)
      return $this->getModel('ca_entities');
    else return $this->entityModel;
  }

  private function getModel($table)
  {
    $modelClient = new ModelService($this->CAurl, $this->CAuser, $this->CAkey, $table, "OPTIONS");
    $modelClient->addGetParameter('lang', $this->locale);
    $result = $modelClient->request();
    // vd_nicely($result);
    return $result->getRawData();
    // return $result;
  }

  /**
   * Build a request body bundle for a Collective Access web API call 
   * for a recording search, and returns the raw data 
   */
  private function querySounds($client)
  {
    $client->setRequestBody(

      array("bundles" => array(
        "ca_objects.idno" => array(),
        "ca_objects.type_id" => array(
          "convertCodesToDisplayText" => true
        ),
        "ca_objects.preferred_labels.name" => array("returnAllLocales" => true, 'returnWithStructure' => true),
        "ca_objects.created.timestamp"  => array(
          "convertCodesToDisplayText" => true
        ),
        "ca_objects.date"  => array(
          "convertCodesToDisplayText" => true,
          "returnAllLocales" => true,
          "returnAsArray" => true
        ),
        "ca_objects.recording_contexts" => array("convertCodesToDisplayText" => true),

        "ca_objects.recording_contexts_note" => array("returnAllLocales" => true, 'returnWithStructure' => true),
        // "ca_objects.description" => array("returnAllLocales" => true),
        "ca_objects.summary" => array('returnAllLocales' => true, 'returnWithStructure' => true),
        "ca_objects.intention" => array("returnAllLocales" => true, 'returnWithStructure' => true),
        "ca_objects.comment" => array("returnAllLocales" => true, 'returnWithStructure' => true),
        "ca_objects.tags" => array("returnAllLocales" => true, 'returnWithStructure' => true),  //retour à la ligne pour séparer les tags
        "ca_objects.lcsh_language" => array(),
        "ca_objects.georeference" => array(),
        "ca_objects.sound_props" => array(),

        "ca_objects.duration" => array(),
        'ca_objects.recording_location' => array("returnAllLocales" => true, 'returnWithStructure' => true),
        "ca_object_representations.idno" => array(),
        "ca_object_representations.mimetype" => array(),
        "ca_object_representations.original_filename" =>  array(),
        "ca_object_representations.media.original.url" =>  array(),
        // "ca_object_representations.media.original.PROPERTIES"=>  array("returnAsArray" => true),
        
        // "ca_object_representations.paths" =>  array("returnAsArray"=>true),


        "ca_collections.related.idno" => array("returnAsArray" => true),
        "ca_collections.related.type_id" => array("convertCodesToDisplayText" => true, "returnAsArray" => true),
        "ca_collections.related.preferred_labels.name" => array("convertCodesToDisplayText" => true, "returnAsArray" => true),
        // to be parsed in different cats with a certain logic : ALBUM / PAROLES / PROJETS / THEMES / SOUNDMAP

        // "ca_entities.related.labels" => array("convertCodesToDisplayText" => true,"returnAsArray" => true),
        "ca_entities.related.idno" => array("returnAsArray" => true),
        "ca_entities.related.entity_id" => array("returnAsArray" => true),
        // "ca_entities.related.displayname" => array("returnAsArray" => true), 
        "ca_entities.related.displayname" => array("returnAsArray" => true),
        "ca_entities" => array("returnAsArray" => true),
        "ca_entities.related.privacy" => array("convertCodesToDisplayText" => true, "returnAsArray" => true),
        "ca_entities" => array("returnAsArray" => true), //display names


        "ca_objects_x_entities.type_id" => array("returnAsArray" => true),
        // "ca_objects_x_entities.type_code" => array(),
        // "ca_entities.related.type_code" => array("returnAsArray" => true),
        // "ca_entities.type_code" => array("returnAsArray" => true),

      ))

    );

    $result = $client->request();
    $rawData = $result->getRawData();
    // vd_nicely($rawData);
    return $rawData;
  }

  /**
   * Mapping between the results of a Collective Access web API call
   * and a ready-to-use array for PW 
   */
  private function mapSoundsData($rawData)
  {

    $recordings = array();
    // vd_nicely($rawData);
    if ($rawData['total'] > 0) {
      $recordings['total'] = $rawData['total'];
      foreach ($rawData['results'] as $rec) {
        $recording=array();
        $recording['id'] = $rec['id'];
        $recording['idno'] = $rec['idno'];
        $recording['type'] = $rec['ca_objects.type_id'];
        $titles = $this->mapAllLocales($rec['ca_objects.preferred_labels.name']);
        $recording['title'] = $titles;

        foreach ($rec['ca_objects.date'] as $date) {
          $date_exploded = explode(';', $date);
          $recording[$date_exploded[1]] = $date_exploded[0];
        }
        $rec['ca_objects.recording_contexts'];
        $context_notes = $this->mapAllLocales($rec['ca_objects.recording_contexts_note']);
        $recording['context_note'] = $context_notes;

        // $recording['description'] = $rec['ca_objects.description'];
        $summaries = $this->mapAllLocales($rec['ca_objects.summary']);
        $recording['summary'] = $summaries;
        if ($rec['ca_objects.intention'] != null) {
          $intentions = $this->mapAllLocales($rec['ca_objects.intention']);
        } else $intentions = '';
        $recording['intention'] = $intentions;
        if ($rec['ca_objects.comment'] != null) {
          $comments = $this->mapAllLocales($rec['ca_objects.comment']);
        } else $comments = '';
        $recording['comment'] = $comments;

        // $recording['tags']=$rec['ca_objects.tags'];
        $tags = $this->mapAllLocales($rec['ca_objects.tags']);
        if ($tags != null) {

          foreach ($tags as $key => $tags_by_lang) {
            $tags_split = preg_split('/\n|\r\n?/', $tags_by_lang, -1,PREG_SPLIT_NO_EMPTY);
            $tags[$key] = $tags_split;
          }
        }
        $recording['tags'] = $tags;
        // $recording['language']=$rec['ca_objects.lcsh_language'];
        $lang = explode(' ', $rec['ca_objects.lcsh_language']);
        $recording['language'] = $lang[0];
        $recording['duration'] = $rec['ca_objects.duration'];
        $locations = $this->mapAllLocales($rec['ca_objects.recording_location']);
        $recording['location'] = $locations;

        $recording['ori_filepath'] = $rec['ca_object_representations.idno'];
        $recording['ori_filename'] = $rec['ca_object_representations.original_filename'];
        $recording['url'] = $rec['ca_object_representations.media.original.url'];
        // $recording['path'] = $rec['ca_object_representations.media.original.path'];
        $recording['mimetype'] = $rec['ca_object_representations.mimetype'];
        $recording['georeference'] = $rec['ca_objects.georeference'];
        $recording['sound_props'] = $rec['ca_objects.sound_props'];
        // $recording['related_collections']=$rec['ca_collections.related.idno'];
        $recording['related_albums'] = array();
        $recording['related_projects'] = array();
        $recording['related_themes'] = array();
        // print_r($rec['ca_collections.related.idno']);
        $related_collections = $rec['ca_collections.related.idno'];
        foreach ($related_collections as $key => $collection) {
          $type = $rec['ca_collections.related.type_id'][$key];
          if ($type == 'Album') {
            $recording['related_albums'][] = $collection;
          }
          if ($type == 'Project') {
            $recording['related_projects'][] = $collection;
          }
          if ($type == 'Theme') {
            $recording['related_themes'][] = $collection;
          }
        }
        // $recording['related_entities']=$rec['ca_entities.related.idno'];
        foreach ($rec['ca_entities.related.idno'] as $key => $entity) {
          $recording['related_entities'][$key]['idno'] = $entity;
          $recording['related_entities'][$key]['id'] = $rec['ca_entities.related.entity_id'][$key];
          $recording['related_entities'][$key]['displayname'] = $rec['ca_entities'][$key];
          $recording['related_entities'][$key]['type'] = $rec['ca_objects_x_entities.type_id'][$key];
          $recording['related_entities'][$key]['type_code'] = $this->objectEntityRelationNames[$rec['ca_objects_x_entities.type_id'][$key]]['type_code'];
          $recording['related_entities'][$key]['typename'] = $this->objectEntityRelationNames[$rec['ca_objects_x_entities.type_id'][$key]]['typename'];
          $recording['related_entities'][$key]['typename_reversed'] = $this->objectEntityRelationNames[$rec['ca_objects_x_entities.type_id'][$key]]['typename_reverse'];
          if(isset($rec['ca_entities.related.privacy'][$key]))
            $recording['related_entities'][$key]['privacy'] = $rec['ca_entities.related.privacy'][$key];
          if(isset($rec['ca_entities.related.privacy'][$key])){
            $recording['related_entities'][$key]['privacy'] = $rec['ca_entities.related.privacy'][$key];
          }else{
            $recording['related_entities'][$key]['privacy'] = 'Yes';
          }
          //here switch off if needed for consultation
          if($recording['related_entities'][$key]['privacy']=='No'){
            $recording['related_entities'][$key]['displayname']=$rec['ca_entities'][$key];
          }else{
            $recording['related_entities'][$key]['displayname']=$rec['ca_entities'][$key];
            // $recording['related_entities'][$key]['displayname']='Anonymous';
          }
            
        }
        $recordings['content'][] = $recording;
      }
      // vd_nicely($tags);
    }
    // vd_nicely($recording);
    return ($recordings);
  }
  private function querySoundsNarrow($client)
  {
    $client->setRequestBody(

      array("bundles" => array(
        "ca_objects.idno" => array(),
        "ca_objects.type_id" => array(
          "convertCodesToDisplayText" => true
        ),
        "ca_objects.preferred_labels.name" => array("returnAllLocales" => true, 'returnWithStructure' => true),
        "ca_object_representations.media.original.url" =>  array(),
       "ca_collections.related.idno" => array("returnAsArray" => true),
        "ca_collections.related.type_id" => array("convertCodesToDisplayText" => true, "returnAsArray" => true),
 

      ))

    );

    $result = $client->request();
    $rawData = $result->getRawData();
    // vd_nicely($rawData);
    return $rawData;
  }

  /**
   * Mapping between the results of a Collective Access web API call
   * and a ready-to-use array for PW 
   */
  private function mapSoundsDataNarrow($rawData)
  {

    $recordings = array();
    // vd_nicely($rawData);
    if ($rawData) {
      $recordings['total'] = $rawData['total'];
      foreach ($rawData['results'] as $rec) {
        $recording['id'] = $rec['id'];
        $recording['idno'] = $rec['idno'];
        $recording['type'] = $rec['ca_objects.type_id'];
        $titles = $this->mapAllLocales($rec['ca_objects.preferred_labels.name']);
        $recording['title'] = $titles;

        
        $recording['url'] = $rec['ca_object_representations.media.original.url'];
       
        $recording['related_albums'] = array();
      
        $related_collections = $rec['ca_collections.related.idno'];
        foreach ($related_collections as $key => $collection) {
          $type = $rec['ca_collections.related.type_id'][$key];
          if ($type == 'Album') {
            $recording['related_albums'][] = $collection;
          }
          if ($type == 'Project') {
            $recording['related_projects'][] = $collection;
          }
          if ($type == 'Theme') {
            $recording['related_themes'][] = $collection;
          }
        }
      
        $recordings['content'][] = $recording;
      }
      // vd_nicely($tags);
    }
    // vd_nicely($recording);
    return ($recordings);
  }


  private function queryAlbums($client)
  {
    $client->setRequestBody(

      array("bundles" => array(
        "ca_collections.idno" => array(),
        "ca_collections.type_id" => array(),
        "ca_collections.preferred_labels.name" => array("returnAllLocales" => true, 'returnWithStructure' => true),
        "ca_collections.created.timestamp"  => array(
          "convertCodesToDisplayText" => true
        ),
        "ca_collections.date"  => array(
          "convertCodesToDisplayText" => true,
          "returnAllLocales" => true,
          "returnAsArray" => true
        ),
        "ca_collections.recording_contexts" => array("convertCodesToDisplayText" => true),

        "ca_collections.recording_contexts_note" => array("returnAllLocales" => true, 'returnWithStructure' => true),
        // "ca_collections.description" => array("returnAllLocales" => true),
        "ca_collections.summary" => array('returnAllLocales' => true, 'returnWithStructure' => true),
        "ca_collections.intention" => array("returnAllLocales" => true, 'returnWithStructure' => true),
        "ca_collections.comment" => array("returnAllLocales" => true, 'returnWithStructure' => true),
        "ca_collections.tags" => array("returnAllLocales" => true, 'returnWithStructure' => true),  //retour à la ligne pour séparer les tags
        "ca_collections.lcsh_language" => array(),
        "ca_collections.georeference" => array(),
        "ca_collections.sound_props" => array(),
        "ca_collections.duration" => array(),
        "ca_collections.attached_file" => array(),
        "ca_collections.file_description" => array(),
        'ca_collections.recording_location' => array("returnAllLocales" => true, 'returnWithStructure' => true),
        "ca_object_representations.idno" => array(),
        "ca_object_representations.mimetype" => array(),
        "ca_object_representations.original_filename" =>  array(),
        "ca_object_representations.media.original.url" =>  array(),
        // "ca_object_representations.media.original.PROPERTIES"=>  array("returnAsArray" => true),



        "ca_objects.related.idno" => array("returnAsArray" => true),
        "ca_objects.related.object_id" => array("returnAsArray" => true),
        "ca_objects.related.type_id" => array("convertCodesToDisplayText" => true, "returnAsArray" => true),
        "ca_objects.related.preferred_labels.name" => array("convertCodesToDisplayText" => true, "returnAsArray" => true),
        // to be parsed in different cats with a certain logic : ALBUM / PAROLES / PROJETS / THEMES / SOUNDMAP

        // "ca_entities.related.labels" => array("convertCodesToDisplayText" => true,"returnAsArray" => true),
        "ca_entities.related.idno" => array("returnAsArray" => true),
        "ca_entities" => array("returnAsArray" => true), //display names

        "ca_objects_x_entities.type_id" => array("returnAsArray" => true)
        // "ca_entities.related.displayname" => array("returnAsArray" => true),

      ))

    );

    $result = $client->request();
    $rawData = $result->getRawData();
    // vd_nicely($rawData);
    return $rawData;
  }
  private function mapAlbumsData($rawData)
  {
    /*     return 
    - title 
    - date creation / modification / recorded
    - summary (FR / NL / ENG ...)
      - comment
    - intention
      - recording context
      - notes sur recording context
      - langue(s) de l'enregistrement
    - coverage :
      - coordonnées geo => peut y avoir des trajets, des zones (cmt CA les enregistre ?)  ?
      - note : recording place
    [- segments liés] (catégories sur des timestamps => sorte de montage)
    - sounds liés (related collections) :
      - IDNO
    */

    $albums = array();
    if ($rawData['total'] > 0) {
      $albums['total'] = $rawData['total'];
      foreach ($rawData['results'] as $rec) {
        $album=array();
        $album['id'] = $rec['id'];
        $album['idno'] = $rec['idno'];
        $album['type'] = $rec['ca_collections.type_id'];
        $titles = $this->mapAllLocales($rec['ca_collections.preferred_labels.name']);
        $album['title'] = $titles;

        foreach ($rec['ca_collections.date'] as $date) {
          $date_exploded = explode(';', $date);
          if(isset($date_exploded[1]))
            $album[$date_exploded[1]] = $date_exploded[0];
        }
        $rec['ca_collections.recording_contexts'];
        $context_notes = $this->mapAllLocales($rec['ca_collections.recording_contexts_note']);
        $album['context_note'] = $context_notes;

        // $album['description'] = $rec['ca_collections.description'];
        $summaries = $this->mapAllLocales($rec['ca_collections.summary']);
        $album['summary'] = $summaries;

        $intentions = $this->mapAllLocales($rec['ca_collections.intention']);
        $album['intention'] = $intentions;

        $comments = $this->mapAllLocales($rec['ca_collections.comment']);
        $album['comment'] = $comments;
        // $album['tags']=$rec['ca_collections.tags'];
        $tags = $this->mapAllLocales($rec['ca_collections.tags']);
        if ($tags != '') {

          foreach ($tags as $key => $tags_by_lang) {
            $tags_split = preg_split('/\n|\r\n?/', $tags_by_lang);
            $tags[$key] = $tags_split;
          }
        }
        $album['tags'] = $tags;
        // $album['language']=$rec['ca_collections.lcsh_language'];
        $lang = explode(' ', $rec['ca_collections.lcsh_language']);
        $album['language'] = $lang[0];
        $album['duration'] = $rec['ca_collections.duration'];
        $locations = $this->mapAllLocales($rec['ca_collections.recording_location']);
        $album['location'] = $locations;
        $album['attached_file']=$rec['ca_collections.attached_file'];
        $album['file_description']=$rec['ca_collections.file_description'];
        $album['ori_filepath'] = $rec['ca_object_representations.idno'];
        $album['ori_filename'] = $rec['ca_object_representations.original_filename'];
        $album['url'] = $rec['ca_object_representations.media.original.url'];
        $album['mimetype'] = $rec['ca_object_representations.mimetype'];
        $album['georeference'] = $rec['ca_collections.georeference'];
        $album['sound_props'] = $rec['ca_collections.sound_props'];
        // $album['related_collections']=$rec['ca_collections.related.idno'];
        $album['related_recordings'] = array();

        // print_r($rec['ca_collections.related.idno']);
        $related_recordings = $rec['ca_objects.related.idno'];
        foreach ($related_recordings as $key => $recording) {
          $type = $rec['ca_objects.related.type_id'][$key];
          if ($type == 'Recording') {
            $album['related_recordings'][] = $recording;
          }
        }
        $album['related_entities'] = $rec['ca_entities.related.idno'];
        // foreach ($rec['ca_entities.related.idno'] as $key => $entity) {
        //   $album['related_entities'][$key]['idno'] = $entity;
        //   // $album['related_entities'][$key]['type'] = $rec['ca_objects_x_entities.type_id'][$key];
        // }
        $albums['content'][] = $album;      
      }
      // vd_nicely($tags);
    }
    return ($albums);
  }
  private function queryAlbumsNarrow($client)
  {
    $client->setRequestBody(

      array("bundles" => array(
        "ca_collections.idno" => array(),
      ))

    );

    $result = $client->request();
    $rawData = $result->getRawData();
    // vd_nicely($rawData);
    return $rawData;
  }
  private function mapAlbumsDataNarrow($rawData)
  {
    /*     return 
    - title 
    - date creation / modification / recorded
    - summary (FR / NL / ENG ...)
      - comment
    - intention
      - recording context
      - notes sur recording context
      - langue(s) de l'enregistrement
    - coverage :
      - coordonnées geo => peut y avoir des trajets, des zones (cmt CA les enregistre ?)  ?
      - note : recording place
    [- segments liés] (catégories sur des timestamps => sorte de montage)
    - sounds liés (related collections) :
      - IDNO
    */

    $albums = array();
    if ($rawData) {
      $albums['total'] = $rawData['total'];
      foreach ($rawData['results'] as $rec) {
        $album['id'] = $rec['id'];
        $album['idno'] = $rec['idno'];
     
        $albums['content'][] = $album;
      }
      // vd_nicely($tags);
    }
    return ($albums);
  }

  private function queryEntities($client)
  {
    $client->setRequestBody(

      array("bundles" => array(
        "ca_entities.idno" => array(),
        "ca_entities.entity_id" => array(),
        "ca_entities.type_id" => array(
          "convertCodesToDisplayText" => true
        ),
        "ca_entities.privacy.preferred_labels" => array(),
        // "ca_entities.preferred_labels" => array(),
        "ca_entities.displayname" => array(),
        "ca_entity_labels.forename" => array(),
        "ca_entity_labels.surname" => array(),
        "ca_entities.biography" => array('returnAllLocales' => true, 'returnWithStructure' => true),
        "ca_entities.address" => array('returnWithStructure' => true),
        "ca_entities.gender" => array(),
        "ca_entities.occupation" => array('returnAllLocales' => true, 'returnWithStructure' => true),
        "ca_entities.moveindate" => array(),
//        "ca_entities.address" => array(),
        "ca_entities.gender" => array(),
        "ca_entities.lcsh_language" => array(),
        "ca_entities.birthdate" => array(),
        "ca_entities.lcsh_country" => array(),
        "ca_entities.telephone" => array(),
        "ca_entities.email" => array(),
        "ca_entities.privacy" => array("convertCodesToDisplayText" => true),

        "ca_objects.related.object_id" => array("returnAsArray" => true),
        "ca_objects.related.idno" => array("returnAsArray" => true),
        // "ca_objects.related.mimetype" => array("returnAsArray" => true),
        
        // "ca_objects.related.id" => array("returnAsArray" => true),
        // "ca_objects.related.type_id" => array("convertCodesToDisplayText" => true,"returnAsArray" => true),
        "ca_objects.related.preferred_labels.name" => array("convertCodesToDisplayText" => true, "returnAsArray" => true),
        // "ca_objects.related.relationship_type_id" => array("returnAsArray" => true),

        // to be parsed in different cats with a certain logic : ALBUM / PAROLES / PROJETS / THEMES / SOUNDMAP

        // "ca_entities.related.labels" => array("convertCodesToDisplayText" => true,"returnAsArray" => true),
        "ca_objects_x_entities.type_id" => array("returnAsArray" => true)

      ))

    );

    $result = $client->request();
    $rawData = $result->getRawData();
//    vd_nicely($rawData);
    return $rawData;
  }

  /**
   * Mapping between the results of a Collective Access web API call
   * and a ready-to-use array for PW 
   */
  private function mapEntitiesData($rawData)
  {

    $entities = array();
    foreach ($rawData['results'] as $rec) {
      // $entity['id'] = $rec['id'];
      //récupérer le total, le dire à Lionel qd c'est fait
      $entity['idno'] = $rec['idno'];
      // $entity['type'] = $rec['ca_objects.type_id'];
      $entity['display_name'] = $rec['display_label'];
      $entity['first_name'] = $rec['ca_entity_labels.forename'];
      $entity['last_name'] = $rec['ca_entity_labels.surname'];
      $entity['related_recordings'] = array();

      // print_r($rec['ca_collections.related.idno']);
      $related_recordings = $rec['ca_objects.related.idno'];
      foreach ($related_recordings as $key => $recording) {
        $entity['related_recordings'][$key]['idno'] = $recording;
      //   $itemClient=new ItemService($this->CAurl, $this->CAuser, $this->CAkey, 'ca_objects', 'GET', $recording);
      //   $itemClient->addGetParameter('lang', $this->locale);
      //   $result = $itemClient->request();
      //   $rawData = $result->getRawData();
      // //   // vd_nicely($rawData['preferred_labels']);
      // //   // vd_nicely($rawData['representations']); 
      // //   // vd_nicely($rawData['ca_objects.date']); 
      // //   // vd_nicely($rawData); 
      //   $entity['related_recordings'][$key]['idno'] = $rawData['intrinsic']['idno']; 
        $entity['related_recordings'][$key]['id'] = $recording;
        $entity['related_recordings'][$key]['type'] = $rec['ca_objects_x_entities.type_id'][$key];
      //   $titles=$this->mapAllLocales2($rawData['preferred_labels']);
      //   $entity['related_recordings'][$key]['title'] = $titles;
      // //   foreach($rawData['representations'] as $representation){
      // //     $entity['related_recordings'][$key]['url'] = $representation['urls']['original'];
      // // //     $entity['related_recordings'][$key]['mimetype'] = $representation['mimetype'];
      // //   }
        
      }

      $entities['content'][] = $entity;
    }
    // vd_nicely($tags);

    return ($entities);
  }
public function exportMapEntities($rawData)
{
    $entities = array();
    foreach ($rawData['results'] as $rec) {
         $entity['id'] = $rec['id'];
        //récupérer le total, le dire à Lionel qd c'est fait
        $entity['idno'] = $rec['idno'];
        $entity['type'] = $rec['ca_entities.type_id'];
        $entity['display_name'] = $rec['display_label'];
        $entity['first_name'] = $rec['ca_entity_labels.forename'];
        $entity['last_name'] = $rec['ca_entity_labels.surname'];
        $entity['biography'] = $this->mapAllLocales($rec['ca_entities.biography']);
        $entity['address'] = $rec['ca_entities.address'];
        $entity['gender'] = $rec['ca_entities.gender'];
        $entity['occupation'] = $this->mapAllLocales($rec['ca_entities.occupation']);
        $entity['language'] = $rec['ca_entities.lcsh_language'];
        $entity['birthdate'] = $rec['ca_entities.birthdate'];
        $entity['country'] = $rec['ca_entities.lcsh_country'];
        $entity['moveindate'] = $rec['ca_entities.moveindate'];
        $entity['telephone'] = $rec['ca_entities.telephone'];
        $entity['email'] = $rec['ca_entities.email'];
        $entity['privacy'] = $rec['ca_entities.privacy'];
//        $entity['related_recordings'] = array();

        // print_r($rec['ca_collections.related.idno']);
        $related_recordings = $rec['ca_objects.related.idno'];
/*        foreach ($related_recordings as $key => $recording) {
            $entity['related_recordings'][$key]['idno'] = $recording;
            //   $itemClient=new ItemService($this->CAurl, $this->CAuser, $this->CAkey, 'ca_objects', 'GET', $recording);
            //   $itemClient->addGetParameter('lang', $this->locale);
            //   $result = $itemClient->request();
            //   $rawData = $result->getRawData();
            // //   // vd_nicely($rawData['preferred_labels']);
            // //   // vd_nicely($rawData['representations']);
            // //   // vd_nicely($rawData['ca_objects.date']);
            // //   // vd_nicely($rawData);
            //   $entity['related_recordings'][$key]['idno'] = $rawData['intrinsic']['idno'];
            $entity['related_recordings'][$key]['id'] = $recording;
            $entity['related_recordings'][$key]['type'] = $rec['ca_objects_x_entities.type_id'][$key];
            //   $titles=$this->mapAllLocales2($rawData['preferred_labels']);
            //   $entity['related_recordings'][$key]['title'] = $titles;
            // //   foreach($rawData['representations'] as $representation){
            // //     $entity['related_recordings'][$key]['url'] = $representation['urls']['original'];
            // // //     $entity['related_recordings'][$key]['mimetype'] = $representation['mimetype'];
            // //   }

        }*/

        $entities['content'][] = $entity;
    }
    // vd_nicely($tags);

    return ($entities);
}

public function exportEntitiesJSON($start, $limit){
    $searchClient = new SearchService($this->CAurl,$this->CAuser,$this->CAkey, 'ca_entities','*');
    $searchClient->addGetParameter('lang', $this->locale);
    $searchClient->addGetParameter('limit', $limit);
    $searchClient->addGetParameter('start', $start);

    $rawData = $this->queryEntities($searchClient);

    $result = $this->exportMapEntities($rawData);
    // vd_nicely($rawData);
    if ($rawData['total'] > 0){
        $file=fopen('json_exports/entitiesJSON.json','x');
        fwrite($file,json_encode($result));
        fclose($file);
        return $result;
    }
}

public function exportAlbumsJSON($start, $limit){
    $searchClient = new SearchService($this->CAurl,$this->CAuser,$this->CAkey, 'ca_collections','*');
    $searchClient->addGetParameter('lang', $this->locale);
    $searchClient->addGetParameter('limit', $limit);
    $searchClient->addGetParameter('start', $start);

    $rawData = $this->queryAlbums($searchClient);

    $result = $this->mapAlbumsData($rawData);
    // vd_nicely($rawData);
    if ($rawData['total'] > 0){
        $file=fopen('json_exports/albumsJSON_'.$start.'-'.$limit.'.json','x');
        fwrite($file,json_encode($result));
        fclose($file);
        return $result;
    }
}
public function exportTracksJSON($start, $limit){
    $searchClient = new SearchService($this->CAurl,$this->CAuser,$this->CAkey, 'ca_objects','*');
    $searchClient->addGetParameter('lang', $this->locale);
    $searchClient->addGetParameter('limit', $limit);
    $searchClient->addGetParameter('start', $start);

    $rawData = $this->querySounds($searchClient);

    $result = $this->mapSoundsData($rawData);
    // vd_nicely($rawData);
    if ($rawData['total'] > 0){
        $file=fopen('json_exports/tracksJSON_'.$start.'-'.$limit.'.json','x');
        fwrite($file,json_encode($result));
        fclose($file);
        return $result;
    }
}
  // END OF - CORE QUERY FUNCTIONS -

  // GET FUNCTIONS

  public function getRecordingByID($id)
  {
    $searchClient = new SearchService($this->CAurl,$this->CAuser,$this->CAkey, 'ca_objects', 'ca_objects.object_id:' . $id);
    $searchClient->addGetParameter('lang', $this->locale);
    $rawData = $this->querySounds($searchClient);
    // vd_nicely($rawData);
    $result = $this->mapSoundsData($rawData);
    if ($rawData['total'] > 0)
    return $result['content'][0];
  }


  /**
   * Returns a clean array of a Collective Access search result of a recording by its IDNO
   */
  public function getRecordingByIDNO($idno)
  {


    // echo ('<h3>=> function getRecordingByIDNO($idno)</h3>');
    // echo ('idno : '. $idno);
    /* TODO: specify request body instead of full request on object */

    $searchClient = new SearchService($this->CAurl,$this->CAuser,$this->CAkey, 'ca_objects', 'ca_objects.idno:' . $idno);
    $searchClient->addGetParameter('lang', $this->locale);
    $rawData = $this->querySounds($searchClient);
    vd_nicely($rawData);
    $result = $this->mapSoundsData($rawData);
    if ($rawData['total'] > 0)
      return $result['content'][0];
  }

  public function getRecordingByIDNONarrow($idno)
  {


    // echo ('<h3>=> function getRecordingByIDNO($idno)</h3>');
    // echo ('idno : '. $idno);
    /* TODO: specify request body instead of full request on object */
    
    $searchClient = new SearchService($this->CAurl, 'ca_objects', 'ca_objects.idno:' . $idno);
    $searchClient->addGetParameter('lang', $this->locale);
    $rawData = $this->querySoundsNarrow($searchClient);
    vd_nicely($rawData);
    $result = $this->mapSoundsDataNarrow($rawData);
    if ($rawData['total'] > 0)
      return $result['content'][0];
  }



  /**
   * Returns a clean array of a Collective Access search on recordings by their tags
   */
  public function getRecordingsbyTag($search, $start, $limit)
  {
    // echo ('<h3>=> function getRecordingsbyTag($search,$start, $limit)</h3>');
    // echo ('search token = "' . $search) . '"<br>';
    $searchClient = new SearchService($this->CAurl, 'ca_objects', 'ca_objects.tags:' . $search);
    $searchClient->addGetParameter('lang', $this->locale);
    $searchClient->addGetParameter('limit', $limit);
    $searchClient->addGetParameter('start', $start);

    $rawData = $this->querySounds($searchClient);

    $result = $this->mapSoundsData($rawData);
    // vd_nicely($rawData);
    if ($rawData['total'] > 0)
      return $result;
  }


  public function getNrecordings($start, $limit)
  {
    // echo ('<h3>=> function getNrecordings($start, $limit)</h3>');
    // echo ('start = ' . $start. '<br>');
    // echo ('limit = ' . $limit. '<br>');
    $searchClient = new SearchService($this->CAurl, $this->CAuser, $this->CAkey, 'ca_objects', '*');
    $searchClient->addGetParameter('lang', $this->locale);
    $searchClient->addGetParameter('limit', $limit);
    $searchClient->addGetParameter('start', $start);
    // $searchClient->addGetParameter('sort', 'idno');
    $searchClient->addGetParameter('sort', 'ca_objects.id');

    $searchClient->addGetParameter('sortDirection', 'desc');
    $rawData = $this->querySounds($searchClient);
    $result = $this->mapSoundsData($rawData);
    // vd_nicely($rawData);
    if ($rawData['total'] > 0)
      return $result;
  }



  public function getRecordingsByAlbum2($id)
  {
    echo ('<h3>=> function getRecordingsbyAlbum2($id)</h3>');
    echo ('id = "' . $id) . '"<br>';
    $searchClient = new SearchService($this->CAurl, 'ca_collections', 'ca_collections.id:' . $id);
    $searchClient->addGetParameter('lang', $this->locale);

    $rawData = $this->queryAlbums($searchClient);
    $result = $this->mapAlbumsData($rawData);
    $related_recordings = array();
    //  vd_nicely($result[0]['related_recordings']);
    foreach ($result['content'][0]['related_recordings'] as $rec) {
      // vd_nicely($rec);
      $related_recordings[] = $this->getRecordingByIDNO($rec);
    }
    // vd_nicely($rawData);
    return $related_recordings;
  }
  public function getRecordingsByAlbum($id)
  {
    echo ('<h3>=> function getRecordingsbyAlbum($id)</h3>');
    echo ('id = "' . $id) . '"<br>';
    $facets = array(
      "album_facet" => [$id]
    );
    return $this->getListingNarrow('ca_objects', $facets, 0, 0, 'asc');
  }

  public function getRecordingsByAlbumIdno($idno){
    $searchClient = new SearchService($this->CAurl, 'ca_collections', 'ca_collections.idno:' . $idno);
    $searchClient->addGetParameter('lang', $this->locale);

    $rawData = $this->queryAlbumsNarrow($searchClient);
    $result = $this->mapAlbumsDataNarrow($rawData);

    return $this->getRecordingsByAlbum($result['content'][0]['id']);


  }


  //-----------------

  /* Collections queries */

  /**
   * returns all albums that contain $search in their summary / intention / comment
   * or albums that contain recordings that have $search in their tags
   */
  public function getAlbumByIDNO($idno)
  {
    // echo ('<h3>=> function getAlbumByIDNO($idno)</h3>');
    // echo ('idno = ' . $idno) . '<br>';
    $searchClient = new SearchService($this->CAurl,$this->CAuser,$this->CAkey, 'ca_collections', 'ca_collections.idno:' . $idno);
    $searchClient->addGetParameter('lang', $this->locale);
    $rawData = $this->queryAlbums($searchClient);
    // vd_nicely($rawData);
    $result = $this->mapAlbumsData($rawData);
    return $result['content'][0];
  }
  public function getAlbumByID($id)
  {
    // echo ('<h3>=> function getAlbumByID($id)</h3>');
    // echo ('id = ' . $id) . '<br>';
    $searchClient = new SearchService($this->CAurl, 'ca_collections', 'ca_collections.id:' . $id);
    $searchClient->addGetParameter('lang', $this->locale);
    $rawData = $this->queryAlbums($searchClient);
    // vd_nicely($rawData);
    $result = $this->mapAlbumsData($rawData);
    if($result['total']>0) return $result['content'][0];
  }
  public function getAlbumsBySearch2($search, $start, $limit)
  {
    // echo ('<h3>=> function getAlbumsBySearch($search, $start, $limit)</h3>');
    // echo ('search token = "' . $search) . '"<br>';
    // echo ('start = ' . $start. '<br>');
    // echo ('limit = ' . $limit. '<br>');

    $searchClient = new SearchService($this->CAurl, 'ca_collections', 'ca_collections.summary:' . $search . ' OR ca_collections.comment:' . $search . ' OR ca_collections.intention:' . $search);

    // is there a way to search on related objects.tags directly in the searchService ? 
    // $searchClient = new SearchService($this->CAurl, 'ca_collections', 'ca_objects.idno:' . $search);


    $searchClient->addGetParameter('lang', $this->locale);

    // we remove these and manage it ourselves 
    // $searchClient->addGetParameter('start', $start);
    // $searchClient->addGetParameter('limit', $limit);

    $rawData = $this->queryAlbums($searchClient);
    // vd_nicely($rawData);
    $result = $this->mapAlbumsData($rawData);
    // merge main results with recsbytag
    $recsByTag = $this->getRecordingsbyTag($search, 0, 0);
    foreach ($recsByTag as $rec) {
      // print_r($rec);
      $related_album = $rec['related_albums'][0];
      // print_r($related_album);
      if (($related_album != null) && (!in_array($related_album, $result))) {
        $album = $this->getAlbumByIDNO($rec['related_albums'][0]);
        // echo('album!!!');
        // print_r($album);
        $r = array_push($result, $album);
        // echo('Pushed new album containing records that have the tag to main result');
      }
    }

    $sliced = array_slice($result, $start, $limit);
    $sliced['total'] = count($result);
    return $sliced;
  }
  // - [ ] public function getAlbumsByTags($search, $start, $limit);

  public function getAlbumsBySearch($search, $start, $limit)
  {
    // echo ('<h3>=> function getAlbumsBySearch($search, $start, $limit)</h3>');
    // echo ('search token = "' . $search) . '"<br>';
    // echo ('start = ' . $start. '<br>');
    // echo ('limit = ' . $limit. '<br>');

    $searchClient = new SearchService($this->CAurl, 'ca_collections', $search);

    // is there a way to search on related objects.tags directly in the searchService ? 
    // $searchClient = new SearchService($this->CAurl, 'ca_collections', 'ca_objects.idno:' . $search);


    $searchClient->addGetParameter('lang', $this->locale);


    $searchClient->addGetParameter('start', $start);
    $searchClient->addGetParameter('limit', $limit);
    $searchClient->addGetParameter('sort', 'ca_collections.dates_value');
    $searchClient->addGetParameter('sortDirection', 'desc');
    $rawData = $this->queryAlbums($searchClient);
    // vd_nicely($rawData);
    $result = $this->mapAlbumsData($rawData);

    return $result;
  }

  public function getNAlbums($start, $limit)
  {
    // echo ('<h3>=> function getNAlbums($start, $limit)</h3>');
    //  echo ('start = ' . $start. '<br>');
    // echo ('limit = ' . $limit. '<br>');
    $searchClient = new SearchService($this->CAurl,$this->CAuser,$this->CAkey, 'ca_collections', 'ca_collections.type_id:115');
    // $searchClient = new SearchService($this->CAurl, 'ca_collections', 'ca_collections.type_id:115 AND ca_objects.related.count:[1-50]');
    $searchClient->addGetParameter('lang', $this->locale);
    $searchClient->addGetParameter('limit', $limit);
    $searchClient->addGetParameter('start', $start);
    //comprends pas pq le sort par date de record fonctionne comme ça !!!
    $searchClient->addGetParameter('sort', 'ca_collections.dates_value');


    $searchClient->addGetParameter('sortDirection', 'desc');

    $rawData = $this->queryAlbums($searchClient);
    $result = $this->mapAlbumsData($rawData);

    // vd_nicely($rawData);
    if ($rawData) {
      // vd_nicely($rawData);
      return $result;
    }
  }

  /**
   * Build a tags list, based on $search
   * You can specify the number of returned tags and the number of max characters for each tag
   */
  public function getTagsAutoCompletion($search, $tags_quantity, $nb_tag_chars)
  {
    $tags_set = array();
    $recs_by_tag = $this->getRecordingsbyTag($search . "*", 0, 0);
    // vd_nicely(($recs_by_tag));
    foreach ($recs_by_tag as $rec) {
      // vd_nicely($rec['tags']);
      foreach ($rec['tags'] as $rec_tags) {
        // print_r($rec_tags);
        foreach ($rec_tags as $tag) {

          // print_r($tag);
          if (strpos($tag, $search)) {
            //alternative possible with array_unique
            if (!in_array($tag, $tags_set)) {
              $truncated_tag = strlen($tag) > $nb_tag_chars ? substr($tag, 0, $nb_tag_chars) . "..." : $tag;
              //ici split au niveau du dernier espace ;)
              $truncated_word_tag = preg_split("/\s+(?=\S*+$)/", $truncated_tag);
              $tags_set[] = $truncated_word_tag[0];
              // echo($truncated_word_tag[0]);
              if (count($tags_set, 0) == $tags_quantity) {
                // echo("i'm here");
                return $tags_set;
              }
            }
          }
        }
      }
    }
    return $tags_set;
  }

  /** WIP testing
   * By track, returns all related tracks by entities, whatever the relation types
   * Don't really need this now
   */
  public function getRelatedRecordings($idno)
  {
    $rec = $this->getRecordingByIDNO($idno);
    //  print_r ($rec['related_entities']);
    $entities = $rec['related_entities'];
    $related_recordings = $this->getRecordingsByEntities($entities);
    //  print_r($related_recordings);
    return $related_recordings;
  }

  //wip ! testing
  public function getRecordingsByEntities($entities)
  {
    $related_recordings = array();
    // print_r($related_recordings);

    foreach ($entities as $entity) {
      $entity_record = $this->getEntityByIDNO($entity['idno']);
      if ($entity_record['related_recordings'] != null) {
        foreach ($entity_record['related_recordings'] as $recording)
          if (!in_array($recording['idno'], $related_recordings))
            // $related_recordings[]=$recording['idno'];
            $related_recordings[] = $recording;
      }
    }

    return $related_recordings;
  }

  //by type creator=110 interviewer=106 interviewee=107
  public function getRecordingsByEntity($idno, $type)
  {
    $related_recordings = [];
    $entity = $this->getEntityByIDNO($idno);
    foreach ($entity['related_recordings'] as $recording) {
      // print_r($recording['type']);
      if ($recording['type'] == $type) {
        // print_r($recording['idno']);
        // $related_record=$this->getRecordingByIDNO($recording['idno']);
        if (!in_array($recording['idno'], $related_recordings))
          // $related_recordings[]=$recording['idno'];
          // $related_recordings[] = $this->getRecordingByID($recording['id']);
          $related_recordings[] = $recording;
      }
    }
    return $related_recordings;
  }

  public function getEntityByIDNO($idno)
  {
    // echo ('<h3>=> function getEntityByIDNO($idno)</h3>');
    // echo ('idno : "'. $idno.'"');
    /* TODO: specify request body instead of full request on object */

    $searchClient = new SearchService($this->CAurl,$this->CAuser,$this->CAkey, 'ca_entities', 'ca_entities.idno:' . $idno);
//    $searchClient = new SearchService($this->CAurl,$this->CAuser,$this->CAkey, 'ca_objects', 'ca_objects.object_id:' . $id);

      $searchClient->addGetParameter('lang', $this->locale);
    $rawData = $this->queryEntities($searchClient);
    // vd_nicely($rawData);
    $result = $this->mapEntitiesData($rawData);
    return $result['content'][0];
  }

  public function getEmptyAlbums()
  {
    // echo ('<h3>=> function getNAlbums($start, $limit)</h3>');
    //  echo ('start = ' . $start. '<br>');
    // echo ('limit = ' . $limit. '<br>');
    $key = 0;
    $start = 0;
    $limit = 200;
    while ($start <= 1600) {


      $searchClient = new SearchService($this->CAurl, 'ca_collections', 'ca_collections.type_id:115');
      // $searchClient = new SearchService($this->CAurl, 'ca_collections', 'ca_collections.type_id:115 AND ca_objects.related.count:[1-50]');
      $searchClient->addGetParameter('lang', $this->locale);
      $searchClient->addGetParameter('limit', $limit);
      $searchClient->addGetParameter('start', $start);
      $searchClient->addGetParameter('sort', 'ca_collections.date');
      $searchClient->addGetParameter('sortDirection', 'desc');

      $rawData = $this->queryAlbums($searchClient);
      $result = $this->mapAlbumsData($rawData);

      // vd_nicely($result);
      if ($rawData)

        foreach ($result as $album) {
          // vd_nicely($album);
          if (count($album['related_recordings']) == 0) {
            $emptyAlbums[$key]['idno'] = $album['idno'];
            $emptyAlbums[$key]['id'] = $album['id'];
            $emptyAlbums[$key++]['title'] = $album['title'];
          }
        }
      $start += $limit;
    }

    //Encode the array into a JSON string.
    $encodedString = json_encode($emptyAlbums);

    //Save the JSON string to a text file.
    file_put_contents('empty_albums_json_array.txt', $encodedString);

    return $emptyAlbums;
  }

  public function getRecordingsWithoutAlbum()
  {
    $key = 0;
    $start = 0;
    $limit = 1000;

    while ($start <= 18256) {
      // echo($start);
      // echo('<br>');
      $searchClient = new SearchService($this->CAurl, 'ca_objects', '*');
      $searchClient->addGetParameter('lang', $this->locale);
      $searchClient->addGetParameter('start', $start);
      $searchClient->addGetParameter('limit', $limit);
      $searchClient->addGetParameter('sort', 'ca_objects.idno');
      $searchClient->addGetParameter('sortDirection', 'desc');
      $rawData = $this->querySounds($searchClient);
      $result = $this->mapSoundsData($rawData);
      foreach ($result as $track) {
        // vd_nicely($album);
        if (count($track['related_albums']) == 0) {
          $tracksWOAlbums[$key]['idno'] = $track['idno'];
          $tracksWOAlbums[$key++]['title'] = $track['title'];
        }
      }
      $start += $limit;
    }
    //Encode the array into a JSON string.
    $encodedString = json_encode($tracksWOAlbums);

    //Save the JSON string to a text file.
    file_put_contents('tracks_wo_album_json_array.txt', $encodedString);

    return $tracksWOAlbums;
  }

  public function deleteAlbums($albums)
  {
    // vd_nicely($albums);
    foreach ($albums as $album) {
      $itemClient = new ItemService($this->CAurl, $this->CAuser, $this->CAkey, 'ca_collections', 'DELETE', $album['id']);
      $result = $itemClient->request();
      $rawData = $result->getRawData();
    }
    return $rawData;
  }


  public function getAllFacetsByName($table, $facet_name){
    $facets = array();
    $allFacets = $this->getFacets($table, $facets);
    print_r($allFacets);
    foreach ($allFacets[$facet_name]['content'] as $facet) {
      $facets[$facet['id']] = $facet['label'];
    }
    return $facets;
  }

  // - [ ] getRecordingsTags() (need db refactor ?)
  // public function getAllTags(){
  //   $facets=array();
  //   $years=array();
  //   $allFacets=$this->getFacets("ca_collections",$facets);
  //   foreach($allFacets['year_facet']['content'] as $year){
  //     $years[]=$year['label'];
  //   }
  //   return $years;
  // }


  //$browseFilter is an array of filter id's
  public function getLanguagesFacets($table, $browseFilter, $facets)
  {
    $newFacets = $this->getFacets($table, $facets);
    $langFacets = array();
    if (isset($newFacets['language_facet']))
      foreach ($newFacets['language_facet']['content'] as $langF) {
        $langFacets[$langF['id']] = $langF['label'];
      }
    if (isset($browseFilter->lang))
      foreach ($browseFilter->lang as $lang) {
        unset($langFacets[$lang]);
      };

    return $langFacets;
  }
  public function getYearsFacets($table, $browseFilter,$facets)
  {

    $newFacets = $this->getFacets($table, $facets);
    $yearFacets = array();
    if (isset($newFacets['year_facet']))
      foreach ($newFacets['year_facet']['content'] as $yearF) {
        $yearFacets[$yearF['id']] = $yearF['label'];
      }
    if (isset($browseFilter->year))
      foreach ($browseFilter->year as $year) {
        unset($yearFacets[$year]);
      };


    return $yearFacets;
  }
  public function getContextFacets($table, $browseFilter, $facets)
  {

    $newFacets = $this->getFacets($table, $facets);
    $contextFacets = array();
    if (isset($newFacets['rec_context_note_facet']))
      foreach ($newFacets['rec_context_note_facet']['content'] as $contextF) {
        $contextFacets[$contextF['id']] = $contextF['label'];
      }
    if (isset($browseFilter->context))
      foreach ($browseFilter->context as $context) {
        unset($contextFacets[$context]);
      };

    return $contextFacets;
  }
  public function getLocationsFacets($table, $browseFilter, $facets)
  {

    $newFacets = $this->getFacets($table, $facets);
    $locationFacets = array();
    if (isset($newFacets['location_facet']))
      foreach ($newFacets['location_facet']['content'] as $locationF) {
        $locationFacets[$locationF['id']] = $locationF['label'];
      }
    if (isset($browseFilter->location))
      foreach ($browseFilter->location as $location) {
        unset($locationFacets[$location]);
      };

    return $locationFacets;
  }
  // END OF - GET FUNCTIONS -

  // FACETS FUNCTIONS
  public function getFacets($table, $facets)
  {

    $browseClient = new BrowseService($this->CAurl, $this->CAuser, $this->CAkey, $table, "OPTIONS");
    $browseClient->addGetParameter('lang', $this->locale);
    //$browseClient->addGetParameter('q'.rand(0,10), 1);
    $browseClient->setRequestBody(
      array("criteria" => $facets)
    );

    $result = $browseClient->request();
    //print_r($result->getRawData());
    for ($i = 0; $i < 3; $i++) {
      if ($result->getRawData())
        break;
      //echo "retry";
      sleep(1);
      $result = $browseClient->request();
    }
    return $result->getRawData();
  }
  public function buildFacets($table,$browseFilter)
  {
    $facets = array();
    if (isset($browseFilter->context))
      foreach ($browseFilter->context as $context) {
        $facets['rec_context_note_facet'][] = $context;
      };
    if (isset($browseFilter->location))
      foreach ($browseFilter->location as $location) {
        $facets['location_facet'][] = $location;
      };
    if (isset($browseFilter->year))
      foreach ($browseFilter->year as $year) {
        $facets['year_facet'][] = $year;
      };
    if (isset($browseFilter->lang))
      foreach ($browseFilter->lang as $lang) {
        $facets['language_facet'][] = $lang;
      };
      $facets_count=$this->getFacets($table,$facets);
      $facets['total']=($table=='ca_collections')?$facets_count['type_facet']['content'][115]['content_count']:
      $facets_count['type_facet']['content'][24]['content_count'];
  
    return $facets;
  }

  public function buildFacetsRequest($facets)
  {
    $request = array("access_facet" => array(1));
    foreach ($facets as $facetKey => $facetArray) {
      $request[$facetKey] = array();
      foreach ($facetArray as $facet) {
        $request[$facetKey][] = $facet['value'];
      }
    }
    return $request;
  }
  public function getListing($table, $facets, $start, $limit, $sort_dir='desc')
  {
    // vd_nicely($facets);
    if ($facets != null) {

      $browseClient = new BrowseService($this->CAurl, $this->CAuser, $this->CAkey, $table, "GET");
      $browseClient->addGetParameter('lang', $this->locale);
      $browseClient->addGetParameter('start', $start);
      $browseClient->addGetParameter('limit', $limit);
      $browseClient->addGetParameter('sort', 'idno');
      $browseClient->addGetParameter('sortDirection', $sort_dir);
      $browseClient->setRequestBody(
        array("criteria" => $facets)
      );
      $result = $browseClient->request();
      $rawData = $result->getRawData();
      // vd_nicely($rawData);
      $results_array = array();
      if ($table == 'ca_collections') {
        foreach ($rawData['results'] as $album) {
          // vd_nicely($album);
          $results_array['content'][] = $this->getAlbumByIDNO($album['idno']);
        }
      } else if ($table == "ca_objects") {
        foreach ($rawData['results'] as $recording) {
          $results_array['content'][] = $this->getRecordingByIDNO($recording['idno']);
        }
      }
    } else {
      if ($table == 'ca_collections')
        $results_array = $this->getNAlbums($start, $limit);
      else if ($table == 'ca_objects')
        $results_array = $this->getNrecordings($start, $limit);
    } // -[x] gérer search et filters concurrents !!!
    $facets_count=$this->getFacets($table,$facets);
    // vd_nicely($facets_count);
      if($table=='ca_collections'){
        $results_array['total']=$facets_count['type_facet']['content'][115]['content_count'];
      }else if($table=='ca_objects'){
        $results_array['total']=$facets_count['type_facet']['content'][24]['content_count'];
      }
    // $results_array['total']=$facets['total'];
    return $results_array;
  }

  public function getListingNarrow($table, $facets, $start, $limit, $sort_dir='desc')
  {
    // vd_nicely($facets);
    if ($facets != null) {
      $facets_count=$this->getFacets('ca_objects',$facets);
      if(isset($facet_count['type_facet'])){

        if($table=='ca_collections'){
          $results_array['total']=$facets_count['type_facet']['content'][115]['content_count'];
        }else if($table=='ca_objects'){
          $results_array['total']=$facets_count['type_facet']['content'][24]['content_count'];
        }
      }
      $browseClient = new BrowseService($this->CAurl, $this->CAuser, $this->CAkey, $table, "GET");
      $browseClient->addGetParameter('lang', $this->locale);
      $browseClient->addGetParameter('start', $start);
      $browseClient->addGetParameter('limit', $limit);
      $browseClient->addGetParameter('sort', 'idno');
      $browseClient->addGetParameter('sortDirection', $sort_dir);
      $browseClient->setRequestBody(
        array(
          "criteria" => $facets,
          "bundles" => array(
            "ca_objects.idno" => array(),
            "ca_objects.type_id" => array(
              "convertCodesToDisplayText" => true
            ),
            "ca_objects.preferred_labels.name" => array("returnAllLocales" => true, 'returnWithStructure' => true),
            "ca_objects.summary" => array('returnAllLocales' => true, 'returnWithStructure' => true),
            "ca_object_representations.media.original.url" =>  array(),
            // "ca_object_representations.media.original.PROPERTIES"=>  array("returnAsArray" => true),
           "ca_collections.related.idno" => array("returnAsArray" => true),
            "ca_collections.related.type_id" => array("convertCodesToDisplayText" => true, "returnAsArray" => true),
    
          )
        )
       
      );
      $result = $browseClient->request();
      $rawData = $result->getRawData();
      vd_nicely($rawData);
      // $results_array = array();
      if ($table == 'ca_collections') {
        foreach ($rawData['results'] as $album) {
          // vd_nicely($album);
          $results_array[] = $this->getAlbumByIDNO($album['idno'])['content'];
        }
      } else if ($table == "ca_objects") {
        foreach ($rawData['results'] as $key=>$rec) {
          $results_array['content'][$key]['id'] = $rec['id'];
          $results_array['content'][$key]['idno'] = $rec['idno'];
          $results_array['content'][$key]['type'] = $rec['ca_objects.type_id'];
          $titles = $this->mapAllLocales($rec['ca_objects.preferred_labels.name']);
          $results_array['content'][$key]['title'] = $titles;
          $results_array['content'][$key]['url'] = $rec['ca_object_representations.media.original.url'];
          $results_array['content'][$key]['summary'] = $rec['ca_objects.summary'];
        }
      }
    } else {
      if ($table == 'ca_collections')
        $results_array = $this->getNAlbums($start, $limit);
      else if ($table == 'ca_objects')
        $results_array = $this->getNrecordings($start, $limit);
    } // -[x] gérer search et filters concurrents !!!

    return $results_array;
  }



  // pour tester la performance de browseService
  // utiliser ceci car on veut un start / limit 
    //by type creator=110 interviewer=106 interviewee=107 orator=108 sound_recordist=105
  public function getRecordingsByEntityBrowse($id, $type, $start, $limit)
  {


    switch ($type) {
      case 106:
        $facets = array(
          "interviewer_facet" => [$id] 
        );
        break;
      case 107:
        $facets = array(
          "interviewee_facet" => [$id] 
        );
        break;
      case 110:
        $facets = array(
          "creator_facet" => [$id] 
        );
        break;
        case 108:
          $facets = array(
            "orator_facet" => [$id] 
          );
          break;
        case 105:
          $facets = array(
            "sound_recordist_facet" => [$id] 
          );
          break;
      default:
        # code...
        break;
    }

    return $this->getListingNarrow('ca_objects', $facets, $start, $limit, 'asc');
  }



  /**
   * pour prendre la traduction de interviewer / interviewee / creator selon le type
   * s'appelle dans la construction de CAcontroller 
   * */
  public function getRecordingEntityRelationNames()
  {

    $relations = array();
    // vd_nicely($this->objectModel);
    if ($this->objectEntityRelationNames == null) {
      foreach ($this->objectModel['sound_recording']['relationship_types']['ca_entities'] as $type) {
        $relations[$type['type_id']]['type_code'] = $type['type_code'];
        $relations[$type['type_id']]['typename'] = $type['typename'];
        $relations[$type['type_id']]['typename_reverse'] = $type['typename_reverse'];
      }
      return $relations;
    } else {
      return $this->objectEntityRelationNames;
    }
  }

  public function measurePerformance($function, $loops)
  {
    $before = microtime(true);

    if ($function == "getRecordingsByEntityBrowse") {
      for ($i = 0; $i < $loops; $i++) {
        $this->getRecordingsByEntityBrowse('ENT.IND.729', 106,0,25);
      }
      $after = microtime(true);
    } else if ($function == 'getRecordingsByEntity') {
      for ($i = 0; $i < $loops; $i++) {
        $this->getRecordingsByEntity('ENT.IND.729', 106);
      }
      $after = microtime(true);
    }
    return ($after - $before) / $i . " sec\n";
  }

  public function countRecordsWithTag($start, $limit)
  {
    $count = 0;
    // $all_rec=$this->getNrecordings(0,18000);
    $all_rec = $this->getNrecordings($start, $limit);
    // vd_nicely($all_rec);
    foreach ($all_rec as $rec) {
      // vd_nicely($rec['tags']);
      if (($rec['tags']) != null) $count++;
    }
    return $count;
  }

  public function countRecordsWithContextNote($start, $limit)
  {
    $count = 0;
    // $all_rec=$this->getNrecordings(0,18000);
    $all_rec = $this->getNrecordings($start, $limit);
    // vd_nicely($all_rec);
    foreach ($all_rec as $rec) {
      // vd_nicely($rec['tags']);
      if (($rec['context_note']) != null) $count++;
    }
    return $count;
  }
  public function countRecordsWithLocation($start, $limit)
  {
    $count = 0;
    // $all_rec=$this->getNrecordings(0,18000);
    $all_rec = $this->getNrecordings($start, $limit);
    // vd_nicely($all_rec);
    foreach ($all_rec as $rec) {
      // vd_nicely($rec['tags']);
      if (($rec['location']) != null) $count++;
    }
    return $count;
  }
  public function countRecordsWithSummary($start, $limit)
  {
    $count = 0;
    // $all_rec=$this->getNrecordings(0,18000);
    $all_rec = $this->getNrecordings($start, $limit);
    // vd_nicely($all_rec);
    foreach ($all_rec as $rec) {
      // vd_nicely($rec['tags']);
      if (($rec['summary']) != null) $count++;
    }
    return $count;
  }
  public function countRecordsWithComment($start, $limit)
  {
    $count = 0;
    // $all_rec=$this->getNrecordings(0,18000);
    $all_rec = $this->getNrecordings($start, $limit);
    // vd_nicely($all_rec);
    foreach ($all_rec as $rec) {
      // vd_nicely($rec['tags']);
      if (($rec['comment']) != null) $count++;
    }
    return $count;
  }
  public function countRecordsWithIntention($start, $limit)
  {
    $count = 0;
    // $all_rec=$this->getNrecordings(0,18000);
    $all_rec = $this->getNrecordings($start, $limit);
    // vd_nicely($all_rec);
    foreach ($all_rec as $rec) {
      // vd_nicely($rec['tags']);
      if (($rec['intention']) != null) $count++;
    }
    return $count;
  }
  public function countAlbumsWithContextNote($start, $limit)
  {
    $count = 0;
    // $all_rec=$this->getNrecordings(0,18000);
    $all_alb = $this->getNAlbums($start, $limit);
    // vd_nicely($all_alb);
    foreach ($all_alb as $alb) {
      // vd_nicely($alb['tags']);
      if (($alb['context_note']) != null) $count++;
    }
    return $count;
  }
  public function countAlbumsWithComment($start, $limit)
  {
    $count = 0;
    // $all_rec=$this->getNrecordings(0,18000);
    $all_alb = $this->getNAlbums($start, $limit);
    // vd_nicely($all_alb);
    foreach ($all_alb as $alb) {
      // vd_nicely($alb['tags']);
      if (($alb['comment']) != null) $count++;
    }
    return $count;
  }
  public function countAlbumsWithSummary($start, $limit)
  {
    $count = 0;
    // $all_rec=$this->getNrecordings(0,18000);
    $all_alb = $this->getNAlbums($start, $limit);
    // vd_nicely($all_alb);
    foreach ($all_alb as $alb) {
      // vd_nicely($alb['tags']);
      if (($alb['summary']) != null) $count++;
    }
    return $count;
  }
  public function countAlbumsWithLocation($start, $limit)
  {
    $count = 0;
    // $all_rec=$this->getNrecordings(0,18000);
    $all_alb = $this->getNAlbums($start, $limit);
    // vd_nicely($all_alb);
    foreach ($all_alb as $alb) {
      // vd_nicely($alb['tags']);
      if (($alb['location']) != null) $count++;
    }
    return $count;
  }
  public function countAlbumsWithIntention($start, $limit)
  {
    $count = 0;
    // $all_rec=$this->getNrecordings(0,18000);
    $all_alb = $this->getNAlbums($start, $limit);
    // vd_nicely($all_alb);
    foreach ($all_alb as $alb) {
      // vd_nicely($alb['tags']);
      if (($alb['intention']) != null) $count++;
    }
    return $count;
  }

  public function getRecordingsBySearch($search, $start, $limit)
  {
    // echo ('<h3>=> function getRecordingsBySearch($search, $start, $limit)</h3>');
    // echo ('search token = "' . $search) . '"<br>';
    // echo ('start = ' . $start. '<br>');
    // echo ('limit = ' . $limit. '<br>');

    $searchClient = new SearchService($this->CAurl, 'ca_objects', $search);

    $searchClient->addGetParameter('lang', $this->locale);


    $searchClient->addGetParameter('start', $start);
    $searchClient->addGetParameter('limit', $limit);
    $searchClient->addGetParameter('sort', 'ca_objects.dates_value');
    $searchClient->addGetParameter('sortDirection', 'desc');
    $rawData = $this->querySounds($searchClient);
    // vd_nicely($rawData);
    $result = $this->mapSoundsData($rawData);

    return $result;
  }

  public function getTags($start, $limit)
  {
    //$start and $limit of the recordings
    $tags=array();
    $recordings = $this->getNrecordings($start, $limit);
    foreach ($recordings['content'] as $recording) {
      $tag=$recording['tags'];
      foreach($tag as $tag_lang){
        $tags=array_merge($tags, $tag_lang);
      }
    }
    return ($tags);
  }




  public function measurePerformance2($function, $loops)
  {
    $before = microtime(true);

    if ($function == "getRecordingsByAlbumIdno") {
      for ($i = 0; $i < $loops; $i++) {
        $this->getRecordingsByAlbumIdno('COL.ALB.1716');
      }
      $after = microtime(true);
    } else if ($function == 'getRecordingsByAlbum') {
      for ($i = 0; $i < $loops; $i++) {
        $this->getRecordingsByAlbum(1739);
      }
      $after = microtime(true);
    }
    return ($after - $before) / $i . " sec\n";
  }

  public function getTracksWithStrangeTitles(){
    
    $search='';
    $searchClient = new SearchService($this->CAurl,$this->CAuser,$this->CAkey, 'ca_objects', $search);

    $searchClient->addGetParameter('lang', $this->locale);

    // $searchClient->addGetParameter('start', $start);
    // $searchClient->addGetParameter('limit', $limit);
    $searchClient->addGetParameter('sort', 'ca_objects.dates_value');
    $searchClient->addGetParameter('sortDirection', 'desc');
    $rawData = $this->querySounds($searchClient);
    // vd_nicely($rawData);
    $result = $this->mapSoundsData($rawData);

    return $result;
  }

  // --- functions to alter DB 
  // get the model from ../import/ca-models

  public function insertRecording($model, $table){
    $this->itemClient->setTable($table);
    $this->itemClient->setRequestMethod('PUT');
    $this->itemClient->setRequestBody($model);

    $result = $this->itemClient->request();

    return($result->getRawData());

  }
  public function updateRecording($model, $table, $id){
    $itemClient=new ItemService($this->CAurl, $this->CAuser, $this->CAkey, $table, 'PUT', $id);
    $itemClient->setTable($table);
    $itemClient->setRequestMethod('PUT');
    $itemClient->setRequestBody($model);
    $itemClient->addGetParameter("id", $id);
    $result = $itemClient->request();

    return($result->getRawData());
  }
  public function deleteRecording($table, $id){
    $this->itemClient->setTable($table);
    $this->itemClient->setRequestMethod('DELETE');
    $this->itemClient->addGetParameter("id", $id);
    $this->itemClient->setRequestBody(json_encode(array('hard'=>'true')));
    
    $result = $this->itemClient->request();
  }
  public function getRecording($table, $id){
    $itemClient=new ItemService($this->CAurl, $this->CAuser, $this->CAkey, $table, 'GET', $id);
      $itemClient->setTable($table);
      $itemClient->setRequestMethod('GET');
      $itemClient->addGetParameter("id", $id);
      // "ca_objects.summary" => array('returnAllLocales' => true, 'returnWithStructure' => true),
      $result = $itemClient->request();

      return($result->getRawData());
  }

  public function getObjectsWithoutMedia(){
    $this->browseClient->setRequestBody(
      array("criteria" =>
        array("has_media_facet"=> array(0)
        )
      )
    );

  }

}