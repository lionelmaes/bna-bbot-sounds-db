<?php

$preloads = array(
    'BrowseService.php',
    'ItemService.php',
    'ModelService.php',
    'SearchService.php'
);

foreach($preloads as $file) {

	require_once($file);
}

unset($preloads);
?>
