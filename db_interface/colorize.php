<?php
// phpinfo();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include('./inc/header.inc');
include('./inc/functions.inc');


$image=imagecreatefromjpeg('test.jpg');
$image1=imagecreatefromjpeg('n1.jpg');
$image2=imagecreatefromjpeg('n2.jpg');
$image3=imagecreatefromjpeg('n3.jpg');

$color=[0,194,162];
$color2=[193, 122, 255];

// $opposite=array(255 - $color[0], 255 - $color[1], 255 - $color[2]);
// $opposite2=array(255 - $color2[0], 255 - $color2[1], 255 - $color2[2]);

// imagefilter($image, IMG_FILTER_GRAYSCALE);
// imagefilter($image, IMG_FILTER_COLORIZE, -$opposite[0], -$opposite[1], -$opposite[2]);
// imagejpeg($image, 'colorize.jpg');
// imagedestroy($image);

// imagefilter($image1, IMG_FILTER_GRAYSCALE);
// imagefilter($image1, IMG_FILTER_COLORIZE, -$opposite2[0], -$opposite2[1], -$opposite2[2]);
// imagejpeg($image1,'colorize2.jpg');
// imagedestroy($image1);

// imagefilter($image2, IMG_FILTER_GRAYSCALE);
// imagefilter($image2, IMG_FILTER_COLORIZE, 47, 199, 116,1);
// imagejpeg($image2, 'colorize3.jpg');
// imagedestroy($image2);



$height=imagesy($image3);
$width=imagesx($image3);


//first we convert the original image to grayscale
imagefilter($image3, IMG_FILTER_GRAYSCALE);


//alpha layer that will be overlayed onto the background color
$alphaLayer = imagecreatetruecolor($width, $height);

imagesavealpha($alphaLayer, true);
imagealphablending($alphaLayer, true);
//set transparent background to alpha layer
imagefill($alphaLayer,0,0,0x7fff0000);

for($x = 0; $x < $width; $x++){
    for($y = 0; $y < $height; $y++){
        $rgb = imagecolorat($image3, $x , $y);
        ///we convert the level of gray (from 0 black to 255 white) to alpha (from 127 transparent to 0 opaque). 
        $alphaVal = 127-(($rgb & 0xFF)/255)*127;
        $newCol = imagecolorallocatealpha($image3, 255, 255, 255, $alphaVal);
        imagesetpixel($alphaLayer, $x, $y, $newCol);
    }
}



//create the destination image and fill it with background color
$destImg=imagecreatetruecolor($width, $height);
imagefill($destImg, 0, 0, imagecolorallocate($destImg, $color[0], $color[1], $color[2]));

//add the alpha layer on top of the background
imagecopy($destImg,$alphaLayer,0,0,0,0,$width,$height);
imagepng($destImg, 'colorize4.png');



?>

<div class="content">
    <div class="result">
    <!-- <h3> Mapped results : <h3> -->
    <!-- <img src="test.jpg" width="500">

    <img src="colorize.jpg" width="500"> -->

    <!-- <img src="n1.jpg" width="500">

    <img src="colorize2.jpg" width="500"> -->

    <!-- <img src="n2.jpg" width="500">

    <img src="colorize3.jpg" width="500"> -->

    <img src="n3.jpg" width="500">

    <img src="colorize4.png" width="500">
    <img src="colorize5.png" width="500">



    </div>
</div>


<?php include('./inc/footer.inc');
?>