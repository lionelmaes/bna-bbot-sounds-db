<?php
class CADB{
  private $pdo = null;

  public function __construct(){

    if(!defined('__CADB_USER__') || !defined('__CADB_KEY__')){
      throw new \Exception('no user or password specified');
    }

    $options = [
    \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
    \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
    \PDO::ATTR_EMULATE_PREPARES   => false,
    ];

    $dsn = "mysql:host=localhost;dbname=bnabbot_ca;charset=latin1";

    try{
     $this->pdo = new \PDO($dsn, __ODB_USER__, __ODB_KEY__, $options);
    }catch (\PDOException $e) {
     throw new \PDOException($e->getMessage(), (int)$e->getCode());
    }
  }


  public function getDeletedTracks(){
    $stmt = $this->pdo->prepare(
      "SELECT ca_object_labels.object_id,name,idno from ca_object_labels,ca_objects
      WHERE ca_object_labels.object_id=ca_objects.object_id
      AND	deleted=1
      ");
    $stmt->execute();

    $data = $stmt->fetchAll();

    return $data;
  }

  public function countCARecordingsStrangeTitles(){
    $stmt = $this->pdo->prepare(
        "SELECT count(ca_objects.idno) from ca_object_labels,ca_objects
        WHERE ca_object_labels.object_id=ca_objects.object_id
        AND	name NOT LIKE '%track%'
        AND name NOT REGEXP '^[0-9,-]+$'
        ");
      $stmt->execute();
  
      $data = $stmt->fetchAll();
  
      return $data;
  }
  public function getCARecordingsStrangeTitles(){
    $stmt = $this->pdo->prepare(
        "SELECT ca_object_labels.object_id,name,idno from ca_object_labels,ca_objects
        WHERE ca_object_labels.object_id=ca_objects.object_id
        AND	name NOT LIKE '%track%'
        AND name NOT REGEXP '^[0-9,-]+$'
        ");
      $stmt->execute();
  
      $data = $stmt->fetchAll();
  
      return $data;
  }

  public function getCARecordings(){
    $stmt = $this->pdo->prepare(
      "SELECT O.object_id, O.idno, OL.name , ORE.original_filename
      FROM ca_objects O, ca_object_labels OL, ca_object_representations ORE, ca_objects_x_object_representations OXOR
      WHERE O.type_id = 24
      AND OL.object_id=O.object_id
      AND O.deleted = 0
      AND O.object_id=OXOR.object_id
      AND OXOR.representation_id=ORE.representation_id
      ORDER BY object_id
      ");
    $stmt->execute();

    $data = $stmt->fetchAll();

    return $data;

  }

  // public function getCARecEmptySummary(){
  //   $stmt = $this->pdo->prepare(
  //     "SELECT ca_object_labels.object_id, name
  //     FROM ca_object_labels, ca_objects, ca_attribute_values, ca_attributes
  //     -- WHERE type_id = 24
      
  //     WHERE NOT EXISTS (
  //       SELECT object_id FROM ca_object_labels WHERE ca_attributes.row_id = ca_objects.object_id AND ca_attribute_values.element_id=31) 
  //     -- AND ca_attributes.row_id=ca_objects.object_id
  //     -- AND ca_attribute_values.attribute_id=ca_attributes.attribute_id
      
  //     -- AND ca_objects.object_id = ca_object_labels.object_id
  //     -- AND ca_objects.deleted=0
  //     -- ORDER BY ca_object_labels.object_id
  //     ");
  //   $stmt->execute();

  //   $data = $stmt->fetchAll();

  //   return $data;
  // }

  public function getCARecordingsNameSummary(){
    $stmt = $this->pdo->prepare(
      "SELECT ca_object_labels.object_id, name, ca_attribute_values.value_longtext1 
      FROM ca_object_labels, ca_objects, ca_attribute_values, ca_attributes
      -- WHERE type_id = 24
      WHERE ca_attributes.row_id=ca_objects.object_id
      AND ca_attribute_values.attribute_id=ca_attributes.attribute_id
      AND ca_attribute_values.element_id=31
      AND ca_objects.object_id = ca_object_labels.object_id
      AND ca_objects.deleted=0
      ORDER BY object_id
      -- LIMIT 0,1000
      ");
    $stmt->execute();

    $data = $stmt->fetchAll();

    return $data;

  }
  public function getCARecordingsNameIntention(){
    $stmt = $this->pdo->prepare(
      "SELECT ca_object_labels.object_id, name, ca_attribute_values.value_longtext1 
      FROM ca_object_labels, ca_objects, ca_attribute_values, ca_attributes
      -- WHERE type_id = 24
      WHERE ca_attributes.row_id=ca_objects.object_id
      AND ca_attribute_values.attribute_id=ca_attributes.attribute_id
      AND ca_attribute_values.element_id=33
      AND ca_objects.object_id = ca_object_labels.object_id
      AND ca_objects.deleted=0
      ORDER BY object_id
      -- LIMIT 0,1000
      ");
    $stmt->execute();

    $data = $stmt->fetchAll();

    return $data;

  }
  public function getCARecordingsNameTags(){
    $stmt = $this->pdo->prepare(
      "SELECT ca_object_labels.object_id, name, ca_attribute_values.value_longtext1 
      FROM ca_object_labels, ca_objects, ca_attribute_values, ca_attributes
      -- WHERE type_id = 24
      WHERE ca_attributes.row_id=ca_objects.object_id
      AND ca_attribute_values.attribute_id=ca_attributes.attribute_id
      AND ca_attribute_values.element_id=35
      AND ca_objects.object_id = ca_object_labels.object_id
      AND ca_objects.deleted=0
      ORDER BY object_id
      -- LIMIT 0,1000
      ");
    $stmt->execute();

    $data = $stmt->fetchAll();

    return $data;

  }

  public function getCARecWithoutSummary(){
    $emptySum=[];
    $allRec=$this->getCARecordings();
    $recWithSummary=$this->getCARecordingsNameSummary();
    // var_dump($recWithSummary);
    
    foreach($allRec as $rec){
      $recIndex[$rec['object_id']]['idno']=$rec['idno'];
      $recIndex[$rec['object_id']]['title']=$rec['name'];
      $recIndex[$rec['object_id']]['file']=$rec['original_filename'];
    }

    // vd_nicely($recIndex);

    foreach($recWithSummary as $rec_sum){
      if(array_key_exists($rec_sum['object_id'],$recIndex)){
        unset($recIndex[$rec_sum['object_id']]);
      }
    }
    // vd_nicely(count($recWithSummary));
    // vd_nicely($recWithSummary);
    // vd_nicely($recIndex);
    // vd_nicely(count($recIndex));

    return $recIndex;


  }
  public function getCARecWithoutIntention(){
    $emptySum=[];
    $allRec=$this->getCARecordings();
    $recWithIntention=$this->getCARecordingsNameIntention();
    // var_dump($recWithSummary);
    
    foreach($allRec as $rec){
      $recIndex[$rec['object_id']]['idno']=$rec['idno'];
      $recIndex[$rec['object_id']]['title']=$rec['name'];
      $recIndex[$rec['object_id']]['file']=$rec['original_filename'];
    }

    // vd_nicely($recIndex);

    foreach($recWithIntention as $rec_sum){
      if(array_key_exists($rec_sum['object_id'],$recIndex)){
        unset($recIndex[$rec_sum['object_id']]);
      }
    }
    // vd_nicely(count($recWithSummary));
    // vd_nicely($recWithSummary);
    // vd_nicely($recIndex);
    // vd_nicely(count($recIndex));

    return $recIndex;


  }
  public function getCARecWithoutTags(){
    $emptySum=[];
    $allRec=$this->getCARecordings();
    $recWithTags=$this->getCARecordingsNameTags();
    // var_dump($recWithSummary);
    
    foreach($allRec as $rec){
      $recIndex[$rec['object_id']]['idno']=$rec['idno'];
      $recIndex[$rec['object_id']]['title']=$rec['name'];
      $recIndex[$rec['object_id']]['file']=$rec['original_filename'];
    }

    // vd_nicely($recIndex);

    foreach($recWithTags as $rec_sum){
      if(array_key_exists($rec_sum['object_id'],$recIndex)){
        unset($recIndex[$rec_sum['object_id']]);
      }
    }
    // vd_nicely(count($recWithSummary));
    // vd_nicely($recWithSummary);
    // vd_nicely($recIndex);
    // vd_nicely(count($recIndex));

    return $recIndex;


  }

  // SELECT OL.object_id,OL.name_sort,AV.value_longtext1,AV.element_id
  // FROM ca_object_labels OL, ca_attributes A, ca_attribute_values AV
  
  // WHERE A.row_id=OL.object_id
  // AND A.attribute_id=AV.attribute_id
  // AND AV.element_id=31

  // SELECT OL.object_id,OL.name_sort,AV.value_longtext1,AV.element_id
  // FROM ca_object_labels OL,ca_attribute_values AV
  // LEFT JOIN ca_attributes 
  // ON ca_attributes.row_id=ca_object_labels.object_id
  // AND	ca_attributes.attribute_id=AV.attribute_id
  // AND AV.element_id=31
  


  public function updateIdnoID($id, $idno, $idnosort){
    echo 'trying to set '.$idno.' <br />';
    try {
    $stmt = $this->pdo->prepare(
      "UPDATE ca_objects
      SET idno = :idno,
      idno_sort = :idnosort
      WHERE object_id = :id
      ");
    $stmt->execute(['idno' => $idno, 'idnosort' => $idnosort, 'id' => $id]);
    }catch (PDOException $e) {
    echo 'HUHOOOO: ' . $e->getMessage();
    }
  }

  public function getLastRecordingID($odbRecord){
    if($odbRecord['Date'] != "0000-00-00 00:00:00"){
      $year = substr($odbRecord['Date'], 0, 4);
    }else if($odbRecord['Date']!= "0000-00-00 00:00:00"){
      $year = substr($odbRecord['Date'], 0, 4);
    }
    if(!isset($year)) $year = date('Y');
    $stmt = $this->pdo->prepare(
      "SELECT *
      FROM ca_objects
      WHERE idno LIKE :search
      ORDER BY object_id
      DESC
      ");
    $stmt->execute(['search' => 'OBJ.REC.'.$year.'%']);
    $data = $stmt->fetchAll();
    if(count($data) == 0)
      return false;


    $lastCARecordParts = explode('.', $data[0]['idno']);
    $lastCARecordID = $lastCARecordParts[count($lastCARecordParts)-1];
    return $lastCARecordID;
  }

  public function getEntitiesByName($firstName,$lastName){
    // echo($firstName.' '.$lastName);
    // $disp=($lastName);
    $disp = utf8_decode($firstName.' '.$lastName);
    $stmt=$this->pdo->prepare(
      "SELECT E.entity_id,E.idno, EL.displayname 
      FROM ca_entities E,ca_entity_labels EL
      Where E.entity_id=EL.entity_id
      AND EL.displayname=:displayname
      ");

    $stmt->execute(['displayname' => $disp]);
    $data = $stmt->fetchAll();
    if(count($data) == 0)
      return false;
    return $data;
  }

  public function getRecordingTitleFile($title,$filename){
    $stmt=$this->pdo->prepare(
      "SELECT O.object_id, O.idno, OL.name , ORE.original_filename
      FROM ca_objects O, ca_object_labels OL, ca_object_representations ORE, ca_objects_x_object_representations OXOR
      WHERE O.type_id = 24
      AND OL.name = :title
      AND ORE.original_filename = :filename
      AND OL.object_id=O.object_id
      AND O.deleted = 0
      AND O.object_id=OXOR.object_id
      AND OXOR.representation_id=ORE.representation_id
      GROUP BY object_id
      ORDER BY object_id
      ");
      $stmt->execute(['title' => $title,'filename' => $filename]);
      $data = $stmt->fetchAll();
      if(count($data) == 0)
        return false;
      return $data;

  }
}

?>
