<?php

//var_dump prettier
function vd_nicely($data){
    return highlight_string("\n\n<?php\n" . var_export($data, true) . ";\n?>");
}

function getTableFromRecArray($id,$data){


    /*Initializing temp variable to design table dynamically*/
    $temp = "<table>";
    
    /*Defining table Column headers depending upon array records*/

    $temp .= "<tr><th>ID</th>";
    $temp .= "<th>Type</th>";
    $temp .= "<th>Reference</th>";
    $temp .= "<th>Date created</th>";
    $temp .= "<th>Date modified</th></tr>";
    
    /*Dynamically generating rows & columns*/
    // for($i = 0; $i < sizeof($data["employees"]); $i++)
    
    vd_nicely($data);
    for($i = 0; $i < sizeof($id); $i++)
    {
    $temp .= "<tr>";
    $temp .= "<td>" . $id . "</td>";
    $temp .= "<td>" . $data["infos"]["type"] . "</td>";
    $temp .= "<td>" . $data["infos"]["ref"] . "</td>";
    $temp .= "<td>" . $data["infos"]["Date created"] . "</td>";
    $temp .= "<td>" . $data["infos"]["Date modified"] . "</td>";
    $temp .= "</tr>";
    }
    
    /*End tag of table*/
    $temp .= "</table>";
    
    /*Printing temp variable which holds table*/
    return $temp;

}

?>