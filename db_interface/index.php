<?php
// phpinfo();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require('./CAInterface.php');
// require('./CAInterface_example.php');
// require('./inc/functions.inc');
include('./inc/header.inc');
require('./config.php');
require('./db/CADB.php');


$cadb = new CADB();
/* connection to API */
// $ca_controller=new CAController('http://dev.bna-bbot.be/ctrl','thy','2GcWJxNAFMvJ64R','en_US', null);
// $ca_controller=new CAController('http://dev.bna-bbot.be/ctrl','god','openthegates','nl_BE', null);
$ca_controller=new CAController('http://dev.bna-bbot.be/ctrl','god','openthegates','fr_BE', null);
//locale : en_US, fr_BE
/* queries testing */
$id=20060;
// $idno='OBJ.REC.1999.IN.4';
// $idno='OBJ.REC.2001.IN.188';
// $idno='OBJ.REC.2021.IN.1';
// $idno='OBJ.REC.2001.IN.44';
// $idno='OBJ.REC.2021.IN.17';
$idno='OBJ.REC.2019.IN.168';
$id=207;
// $idno='OBJ.REC.2001.IN.800';

// TEST QUERIES

// $result = $ca_controller->getTags(0,500);
// $result = $ca_controller->getTags(500,500);
// $result = $ca_controller->getTags(1000,500);
// $result = $ca_controller->getTags(1500,500);
// $result = $ca_controller->getTags(2000,500);
// $result = $ca_controller->getTags(2500,500);
// $result = $ca_controller->getTags(3000,500);
// $result = $ca_controller->getTags(3500,500);

//$result = $ca_controller->getRecordingByID($id);
// $result = $ca_controller->getRecordingByIDNO($idno);
// $result=$cadb->countCARecordingsStrangeTitles(); //1352 records
// $result=$cadb->getCARecordingsStrangeTitles();
// $result = $ca_controller->getTracksWithStrangeTitles();
 $result = $ca_controller->getNrecordings(55,20);
// $result = $cadb->getEntitiesByName('Flavien', 'Gillié');
// $result = $ca_controller->getRecordingsbyTag('tag1', 0, 0);
// $result = $ca_controller->getRecordingsbyTag('maman', 0, 0);
// $result = $ca_controller->getTagsAutoCompletion('avoir', 50, 2000);


// $result = $ca_controller->getAlbumsBySearch('test', 0, 10);
// $result = $ca_controller->getAlbumsBySearch('avoir', 0, 10);
// $result = $ca_controller->getRecordingByID('18981');

// $result = $ca_controller->getAlbumByIDNO('COL.ALB.1742');

// $result = $ca_controller->getNAlbums(0, 100);

// $result = $ca_controller->getRecordingsbyAlbum(18);
// $result = $ca_controller->getRecordingsbyAlbumIdno('COL.ALB.1716');
// $result = $ca_controller->getAlbumByID(1726);

// $result = $ca_controller->getEntityByIDNO('ENT.IND.729');

// $result = $ca_controller->getRelatedRecordings($idno);

// $result = $ca_controller->getTagsAutoCompletion('tagine', 10, 30);
// $result = $ca_controller->getEntityModel();


// $result=$ca_controller->getEmptyAlbums();
// $result=$ca_controller->getEmptyAlbums(901,700);
// $result=$ca_controller->getEmptyAlbums(1601,119);

// $result=$ca_controller->getRecordingsWithoutAlbum();
// $albums=$ca_controller->getEmptyAlbums();
// $result=$ca_controller->deleteAlbums($albums);


//besoin de gérer plusieurs médias pour 1 record ?
// $result = $ca_controller->getMediaFromRec($id);
// $result = $ca_controller->getEntityModel();

// $result = $ca_controller->getObjectDetail($id);
// $result = $ca_controller->getEntityDetail($id);
// $result = $ca_controller->getObjectModel();
// $result = $ca_controller->getRecordingEntityRelationNames();
// $result = $ca_controller->getModel('ca_objects');

//TEST PERFORMANCE
// $result = $ca_controller->getRecordingsByEntity('ENT.IND.1642', 107);
// $result = $ca_controller->getRecordingsByEntityBrowse(335,106,0,0);
// $result = $ca_controller->getRecordingsByEntityBrowse(1998,109,0,0);
// $result = $ca_controller->measurePerformance('getRecordingsByEntityBrowse',3); // time elapsed : 26.858118375142 sec
// $result = $ca_controller->getRecordingsByEntity('ENT.IND.729',106);
// $result = $ca_controller->measurePerformance('getRecordingsByEntity',1); // time elapsed : 25.002594312032 sec
// $result = $ca_controller->measurePerformance2('getRecordingsByAlbum',50); // time elapsed : 0.81763134002686 sec
// $result = $ca_controller->measurePerformance2('getRecordingsByAlbumIdno',50); // time elapsed : 0.97270097732544 sec



//testing facets
$facets=array(
    // "year_facet"=>[2009,2010],
    "year_facet"=>[1999],
    //  "rec_context_note_facet"=>[212900, 16082],
    // "language_facet"=>[12]
    // "language_facet"=>[181596]
    // "location_facet"=>[168493]
);

// $result = $ca_controller->getFacets("ca_objects",$facets);
// $result = $ca_controller->buildFacetsRequest($facets);
// $result = $ca_controller->getListing("ca_collections", $facets,0,0);

// $result = $ca_controller->getAlbumsYears();
// $result = $ca_controller->getAlbumsPlaces();
// $result = $ca_controller->getAlbumsLanguages();

// $result = $ca_controller->getRecordingsBySearch("palace", 0,1000);
// $result = $ca_controller->getRecordingsByEntityBrowse('ENT.IND.729',106);
// $result = $ca_controller->getRecordingsByEntity('ENT.IND.729',106);


//AUDIT
//audit tracks : tags
// for($i=0;$i<18;$i++){
//     $result[]=$ca_controller->countRecordsWithTag(1000*$i,1000);
// }
// $result['total']=array_sum($result); // sur 18218

// //audit tracks : context_note
// for($i=0;$i<18;$i++){
    //     $result[]=$ca_controller->countRecordsWithContextNote(1000*$i,1000);
    // }
    // $result['total']=array_sum($result); // sur 18218
    
    // //audit tracks : summary
    // for($i=0;$i<18;$i++){
//     $result[]=$ca_controller->countRecordsWithSummary(1000*$i,1000);
// }
// $result['total']=array_sum($result); // sur 18218

// //audit tracks : comment
// for($i=0;$i<18;$i++){
//     $result[]=$ca_controller->countRecordsWithComment(1000*$i,1000);
// }
// $result['total']=array_sum($result); // sur 18218

// // audit tracks : intention
// for($i=0;$i<18;$i++){
//     $result[]=$ca_controller->countRecordsWithIntention(1000*$i,1000);
// }
// $result['total']=array_sum($result); // sur 18218

// //audit tracks : location
// for($i=0;$i<18;$i++){
    //     $result[]=$ca_controller->countRecordsWithLocation(1000*$i,1000);
    // }
    // $result['total']=array_sum($result); // sur 18218
    
    
    //audit album : context_note
    // for($i=0;$i<16;$i++){
        //     $result[]=$ca_controller->countAlbumsWithContextNote(100*$i,100);
        // }
// $result['total']=array_sum($result); // sur 1595

// audit album : summary
// for($i=0;$i<16;$i++){
//     $result[]=$ca_controller->countAlbumsWithSummary(100*$i,100);
// }
// $result['total']=array_sum($result); // sur 1595

// audit album : location
// for($i=0;$i<16;$i++){
//     $result[]=$ca_controller->countAlbumsWithLocation(100*$i,100);
// }
// $result['total']=array_sum($result); // sur 1595

// // audit album : intention
// for($i=0;$i<16;$i++){
//     $result[]=$ca_controller->countAlbumsWithIntention(100*$i,100);
// }
// $result['total']=array_sum($result); // sur 1595

// // audit album : comment
// for($i=0;$i<16;$i++){
//     $result[]=$ca_controller->countAlbumsWithComment(100*$i,100);
// }
// $result['total']=array_sum($result); // sur 1595


/* testing export to JSON */
// $result = $ca_controller->exportEntitiesJSON(0,0);

//$result = $ca_controller->exportAlbumsJSON(1,1000);
//$result = $ca_controller->exportAlbumsJSON(1001,1000);
//$result = $ca_controller->exportAlbumsJSON(0,1);


//$result = $ca_controller->exportTracksJSON(0,1);
//$result = $ca_controller->exportTracksJSON(1,1000);
//$result = $ca_controller->exportTracksJSON(1001,1000);
//$result = $ca_controller->exportTracksJSON(2001,1000);
//$result = $ca_controller->exportTracksJSON(3001,1000);
//$result = $ca_controller->exportTracksJSON(4001,1000);
//$result = $ca_controller->exportTracksJSON(5001,1000);
//$result = $ca_controller->exportTracksJSON(6001,1000);
//$result = $ca_controller->exportTracksJSON(7001,1000);
//$result = $ca_controller->exportTracksJSON(8001,1000);
//$result = $ca_controller->exportTracksJSON(9001,1000);
//$result = $ca_controller->exportTracksJSON(10001,1000);
//$result = $ca_controller->exportTracksJSON(11001,1000);
//$result = $ca_controller->exportTracksJSON(12001,1000);
//$result = $ca_controller->exportTracksJSON(13001,1000);
//$result = $ca_controller->exportTracksJSON(14001,1000);
//$result = $ca_controller->exportTracksJSON(15001,1000);
//$result = $ca_controller->exportTracksJSON(16001,1000);
//$result = $ca_controller->exportTracksJSON(17001,1000);
//$result = $ca_controller->exportTracksJSON(18001,1000);


?>

<div class="content">
    <div class="result">
    <h3> Mapped results : <h3>
    
    <?php vd_nicely($result); ?>
    
    </div>
</div>


<?php include('./inc/footer.inc');
?>

