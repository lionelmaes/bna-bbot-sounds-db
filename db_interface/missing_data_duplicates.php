<?php
// phpinfo();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require('./CAInterface.php');
// require('./CAInterface_example.php');
// require('./inc/functions.inc');

include('./inc/header.inc');
require('./config.php');
require('./db/CADB.php');
require('./db/ODB.php');


$cadb = new CADB();
$odb = new ODB();

/* connection to API */

$ca_controller=new CAController('http://dev.bna-bbot.be/ctrl','god','openthegates','fr_BE', null);


$count_foundCADB=0;
$odbContacts=$odb->getContacts();
foreach($odbContacts as $key => $contact){
    // $fname=htmlentities($contact['FirstName']);
    // $lname=htmlentities($contact['LastName']);

    $fname=html_entity_decode($contact['FirstName']);
    $lname=html_entity_decode($contact['LastName']);
    // var_dump($fname);
    // vd_nicely(($fname).' '.$lname);
    $cadb_entities=$cadb->getEntitiesByName($fname,$lname); 
    if($cadb_entities){
        foreach($cadb_entities as $entity){
            $odbContacts[$key]['cadb_ids'][]=$entity['entity_id'];
            $count_foundCADB++;
            // echo('ID: '.$entity['entity_id'].' IDNO: '.$entity['idno'].' DisplayName: '.$entity['displayname'].'<br>');
        }
    }
}
// vd_nicely($odbContacts);
echo('count total contacts in ODB : '.count($odbContacts).'<br>');
echo('count found CADB corresponding contacts : '.$count_foundCADB.'<br>');

$odb_duplicates = $odb->getDuplicates();
// 1695
// vd_nicely($odb_duplicates);
$count=0;
// change values here to do the reparation
// comment / uncomment the update CA line farther in the code to do a dry-run or a real update run 
$start=0;
$limit=0;
$log  = PHP_EOL.
        'REPAIRING RELATED ENTITIES from ODB to CADB'.PHP_EOL.
        '===================================='.PHP_EOL.
        'This is a run from recordings with start='.$start.' and limit='.($limit).PHP_EOL.
        'We take all the duplicates recordings (with same file path but simetimes different titles) in ODB and accordingly applying the missing related entities in CADB.';

$duplicates=[];
foreach($odb_duplicates as $key=>$sound){
    $duplicates[$key]['ids']=explode(',',$sound['GROUP_CONCAT(id)']);
    $duplicates[$key]['titles']=explode(',',$sound['GROUP_CONCAT(title)']);
    $path=explode('/',$sound['file']);
    $duplicates[$key]['filename']=end($path);
}
// vd_nicely($duplicates);
for($key=$start;$key<($start+$limit); $key++){
    // echo($key);
    // echo($duplicates[$key]['titles'][0]);
    $try=$cadb->getRecordingTitleFile($duplicates[$key]['titles'][0],$duplicates[$key]['filename']);
    $try2=$cadb->getRecordingTitleFile($duplicates[$key]['titles'][1],$duplicates[$key]['filename']);
    // echo('<br>1) ');
    if($try!=false){
        $check=$try;
        $usable=0;
        $complete=1;
    }else if ($try2!=false){
        $check=$try2;
        $usable=1;
        $complete=0;
    }else $check=false;
    // vd_nicely($check);
    // echo('<br>2) ');
    // vd_nicely($check2);

    // take the usable one of the 2 (get id CADB)
    
    // $usable=0;
    // take the complete rec id of ODB (the other one than CADB) 
    // relationship between odb <-> cadb ids in $odbContacts
    if($check!=false){

        echo('<br>id ODB : '.$duplicates[$key]['ids'][$complete].'<br>');
        echo('<br>Corresponding incomplete sound CADB id : '.$check[0]['object_id']);
        echo('<br>=================<br>');
    
        $found_odb_rel=$odb->getRelatedContactsByRec($duplicates[$key]['ids'][$complete]);
        // get dependencies 32 or 33 from ODB rec id and build a model
        // insert the model - new relationship in CADB
            // use the algo from missing related_entities
        // write the log file
        // vd_nicely($found_odb_rel);
        // vd_nicely($relatedEntitiesCA);
        foreach($found_odb_rel as $rel){
            $indexContact=array_search($rel['TargetID'],array_column($odbContacts,'ID'));
            // vd_nicely($odbContacts[$indexContact]);
            if($indexContact!==false)
                $odb_entity=$odbContacts[$indexContact];
            else $odb_entity=false;
            if(($odb_entity!==false) && isset($odb_entity['cadb_ids'])){
            // foreach($odb_entity['cadb_ids'] as $cadb_id){
            //     if(!array_search($cadb_id, array_column($relatedEntitiesCA,'id'))){
            //         $missing=1;
            //         $missing_id=$cadb_id;
            //         $missing_type=$rel['DependencyTypeID'];
            //     }else{
            //         $missing=0;
            //     }
            // }
            // vd_nicely(($check[0]['object_id']));
            $cur_rec=$ca_controller->getRecordingByID($check[0]['object_id']);
            // vd_nicely(($cur_rec));
            $relatedEntitiesCA=$cur_rec['related_entities'];
                if($relatedEntitiesCA!==false){
                    foreach($odb_entity['cadb_ids'] as $cadb_id){
                        $search=array_search($cadb_id, array_column($relatedEntitiesCA, 'id'));
                        if($search!==false){
                            $missing_id=-1;
                            $missing_type=-1;
                            $missing=false;
                            break;
                            
                        } else{
                            $missing_id=$cadb_id;
                            $missing_type=$rel['DependencyTypeID'];
                            $missing=true;
                            
                        }
                    }
                
                }else $search=-1;
    
                var_dump($search);
                // if ($search===false) {
                    
                //         $missing = true;
                //         $missing_type = $rel['DependencyTypeID'];
                // } else {
                //         $missing = false;
                //     }
                
                   
    
    
                if($missing){
                    echo('<br>!!! MISSING :'.$missing_id.' with type :'.$missing_type.'<br>');
                    vd_nicely($odb_entity);
                    //adding in model
                    $objectModel['related']['ca_entities'][0]=[
                        'type_id' => ($missing_type==32)?106:107,
                        'entity_id' => $missing_id,
                        'item_type_id' => 83
                    ];
                    //update CADB
                    // uncomment here to update / leave comment to dry-run
                    // vd_nicely($objectModel);
    
                    // $update=$ca_controller->updateRecording($objectModel,'ca_objects',$check[0]['object_id']);
    
                    // var_dump($update);
    
                    
                    // write to log
                    $log  = date("F j, Y, g:i a").PHP_EOL.
                    "Updating CA record - id: ".$check[0]['object_id']." / title: ".$check[0]['name']." / file: ".$check[0]['original_filename'].PHP_EOL.
                    "Related entities : ".PHP_EOL;
                    //foreach entities...
                    $log.=
                    "From ODB:".PHP_EOL.
                    "   ID: ".$duplicates[$key]['ids'][$complete].PHP_EOL.
                    "   Title: ".$duplicates[$key]['titles'][$complete].PHP_EOL.
                    "   File: ".$duplicates[$key]['filename'].PHP_EOL.
                    "   Related entity: ".$missing_id.PHP_EOL.
                    "   Relation type: ".$missing_type.PHP_EOL.
                    "-------------------------".PHP_EOL;
                    //Save string to log, use FILE_APPEND to append.
                    file_put_contents('./log_duplicates_'.date("j.n.Y").'_'.$start.'_'.$limit.'.log', $log, FILE_APPEND);
            
                    $count++;
                }
            }else{
                // $log = 'Careful : This person is not in CADB !'.PHP_EOL.
                // 'ODB id: '.$odb_entity['ID']." / ODB name: ".$odb_entity['FirstName'].' '.$odb_entity['LastName'].PHP_EOL;
                // file_put_contents('./log_entities_'.date("j.n.Y").'.log', $log, FILE_APPEND);
    
                echo('<br>Careful : This person is not in CADB !<br>');
                echo('ODB id: '.$odb_entity['ID']." / ODB name: ".$odb_entity['FirstName'].' '.$odb_entity['LastName'].'<br>');
            }
    }

    }



}
// vd_nicely($duplicates);
        










        $log="A total of ".$count." entities records have been processed.".PHP_EOL.
     "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~";
file_put_contents('./log_duplicates_'.date("j.n.Y").'_'.$start.'_'.$limit.'.log', $log, FILE_APPEND);
vd_nicely($count);

// $test = $cadb->getRecordingTitleFile('1375 - Track 03','Pleinopenair_track_03.mp3');
// vd_nicely($test);
// $test = $cadb->getRecordingTitleFile('1662---04---BNA-BBOT','1662---04---BNA-BBOT.mp3');
// vd_nicely($test);
// $test = $cadb->getRecordingTitleFile('1443 - Track 04','Mr-Bal---04---les-bieres-des-ouvriers.mp3');
// vd_nicely($test);



?>

<div class="content">
    <div class="result">
    <!-- <h3> Mapped results : <h3> -->
    
    <?php //vd_nicely($result); ?>
    <p>This script repairs the related entities from ODB to CADB</p>
    From $start to $limit.
    Operations are logged to log file. 

    </div>
</div>


<?php include('./inc/footer.inc');
?>

