<?php
// phpinfo();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require('./CAInterface.php');
// require('./CAInterface_example.php');
// require('./inc/functions.inc');

include('./inc/header.inc');
require('./config.php');
require('./db/CADB.php');
require('./db/ODB.php');


$cadb = new CADB();
$odb = new ODB();

/* connection to API */

$ca_controller=new CAController('http://dev.bna-bbot.be/ctrl','god','openthegates','fr_BE', null);






// $odb_dependencies = $odb->getRecordsRelatedContacts();
// vd_nicely($odb_dependencies);
$count_foundCADB=0;
$odbContacts=$odb->getContacts();
foreach($odbContacts as $key => $contact){
    // $fname=htmlentities($contact['FirstName']);
    // $lname=htmlentities($contact['LastName']);

    $fname=html_entity_decode($contact['FirstName']);
    $lname=html_entity_decode($contact['LastName']);
    // var_dump($fname);
    // vd_nicely(($fname).' '.$lname);
    $cadb_entities=$cadb->getEntitiesByName($fname,$lname); 
    if($cadb_entities){
        foreach($cadb_entities as $entity){
            $odbContacts[$key]['cadb_ids'][]=$entity['entity_id'];
            $count_foundCADB++;
            // echo('ID: '.$entity['entity_id'].' IDNO: '.$entity['idno'].' DisplayName: '.$entity['displayname'].'<br>');
        }
    }
}
// vd_nicely($odbContacts);
echo('count total contacts in ODB : '.count($odbContacts).'<br>');
echo('count found CADB corresponding contacts : '.$count_foundCADB.'<br>');


// $tracks=$cadb->getCARecordings();
// $start=56;
// $limit=2;
$start=0;
$limit=1000;
$tracks=$ca_controller->getNrecordings($start,$limit);
// vd_nicely($tracks);
// $objectModel=json_decode(file_get_contents('../import/ca-models/recording_orig2.json'), True);

$objectModel=array('related'=>
array(
    'ca_entities' => 
    array (
    //   0 => 
    //   array (
    //       'type_id' => 106,
    //     'entity_id' => 2001,
    //     'item_type_id' => 83
    //   ),
    //   1 =>
    //   array (
    //       'type_id' => 107,
    //     'entity_id' => 2002,
    //     'item_type_id' => 83
    //   ),
    )
));

// 'objectinterviewee' => 107,
// 'objectinterviewer' => 106

// vd_nicely($objectModel);
// $result=$ca_controller->updateRecording($objectModel,'ca_objects',16959);
// $result=$ca_controller->getRecording('ca_objects',562);
// vd_nicely($result);
$count=0;


$log  = PHP_EOL.
        'REPAIRING RELATED ENTITIES from ODB to CADB'.PHP_EOL.
        '===================================='.PHP_EOL.
        'This is a run from recordings with start='.$start.' and limit='.($limit).PHP_EOL;

file_put_contents('./log_entities_'.date("j.n.Y").'.log', $log, FILE_APPEND);

foreach($tracks['content'] as $key=>$track){
        echo('<br><br>id CA: ');
        echo($track['id']);
        // vd_nicely($track);


        if(isset($track['title']['FR']))
            $title=$track['title']['FR'];
        else if(isset($track['title']['EN']))
            $title=$track['title']['EN'];
        else if(isset($track['title']['NL']))
            $title=$track['title']['NL'];

        $context_note='';
        if(isset($track['context_note']['FR']))
            $context_note=$track['context_note']['FR'];
        else if(isset($track['context_note']['EN']))
            $context_note=$track['context_note']['EN'];
        else if(isset($track['context_note']['NL']))
            $context_note=$track['context_note']['NL'];

        $track_odb=$odb->getTrackIdBodyFromCATitleSimple($title,$context_note,$track['ori_filename']);
        if(isset($track['related_entities']))
            $relatedEntitiesCA=$track['related_entities'];
        else
            $relatedEntitiesCA=false;
        // vd_nicely($relatedEntitiesCA);
        var_dump($track_odb);
        $missing=false;
        if($track_odb){
            // vd_nicely($track_odb);
            echo('<br>id ODB : '.$track_odb[0]['ID'].'<br>');
            echo('<br>=================<br>');
            $found_odb_rel=$odb->getRelatedContactsByRec($track_odb[0]['ID']);
            vd_nicely($found_odb_rel);
            vd_nicely($relatedEntitiesCA);
            foreach($found_odb_rel as $rel){
                $indexContact=array_search($rel['TargetID'],array_column($odbContacts,'ID'));
                // vd_nicely($odbContacts[$indexContact]);
                if($indexContact!==false)
                    $odb_entity=$odbContacts[$indexContact];
                else $odb_entity=false;
                if(($odb_entity!==false) && isset($odb_entity['cadb_ids'])){
                // foreach($odb_entity['cadb_ids'] as $cadb_id){
                //     if(!array_search($cadb_id, array_column($relatedEntitiesCA,'id'))){
                //         $missing=1;
                //         $missing_id=$cadb_id;
                //         $missing_type=$rel['DependencyTypeID'];
                //     }else{
                //         $missing=0;
                //     }
                // }
                    if($relatedEntitiesCA!==false){
                        foreach($odb_entity['cadb_ids'] as $cadb_id){
                            $search=array_search($cadb_id, array_column($relatedEntitiesCA, 'id'));
                            if($search!==false){
                                $missing_id=-1;
                                $missing_type=-1;
                                $missing=false;
                                break;
                                
                            } else{
                                $missing_id=$cadb_id;
                                $missing_type=$rel['DependencyTypeID'];
                                $missing=true;
                                
                            }
                        }
                    
                    }else $search=-1;

                    var_dump($search);
                    // if ($search===false) {
                        
                    //         $missing = true;
                    //         $missing_type = $rel['DependencyTypeID'];
                    // } else {
                    //         $missing = false;
                    //     }
                    
                       


                    if($missing){
                        echo('<br>!!! MISSING :'.$missing_id.' with type :'.$missing_type.'<br>');
                        vd_nicely($odb_entity);
                        //adding in model
                        $objectModel['related']['ca_entities'][0]=[
                            'type_id' => ($missing_type==32)?106:107,
                            'entity_id' => $missing_id,
                            'item_type_id' => 83
                        ];
                        //update CADB
                        // uncomment here to update / leave comment to dry-run
                        // vd_nicely($objectModel);

                        // $update=$ca_controller->updateRecording($objectModel,'ca_objects',$track['id']);

                        // var_dump($update);

                        
                        // write to log
                        $log  = date("F j, Y, g:i a").PHP_EOL.
                        "Updating CA record - id: ".$track['id']." / title: ".$title." / file: ".$track['ori_filename'].PHP_EOL.
                        "Related entities : ".PHP_EOL;
                        //foreach entities...
                        $log.=
                        "From ODB:".PHP_EOL.
                        "   ID: ".$track_odb[0]['ID'].PHP_EOL.
                        "   Title: ".$track_odb[0]['Title'].PHP_EOL.
                        "   File: ".$track_odb[0]['File'].PHP_EOL.
                        "   Related entity: ".$missing_id.PHP_EOL.
                        "   Relation type: ".$missing_type.PHP_EOL.
                        "-------------------------".PHP_EOL;
                        //Save string to log, use FILE_APPEND to append.
                        file_put_contents('./log_entities_'.date("j.n.Y").'.log', $log, FILE_APPEND);
                
                        $count++;
                    }
                }else{
                    // $log = 'Careful : This person is not in CADB !'.PHP_EOL.
                    // 'ODB id: '.$odb_entity['ID']." / ODB name: ".$odb_entity['FirstName'].' '.$odb_entity['LastName'].PHP_EOL;
                    // file_put_contents('./log_entities_'.date("j.n.Y").'.log', $log, FILE_APPEND);

                    echo('<br>Careful : This person is not in CADB !<br>');
                    echo('ODB id: '.$odb_entity['ID']." / ODB name: ".$odb_entity['FirstName'].' '.$odb_entity['LastName'].'<br>');
                }
            }


        }
  
    

    
    // if($key>($start+$limit)) break;

}
$log="A total of ".$count." records have been processed.".PHP_EOL.
     "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~";
file_put_contents('./log_entities_'.date("j.n.Y").'.log', $log, FILE_APPEND);
vd_nicely($count);

// var_dump(count($tracks));
// $result = $tracks;
// $result=$track_odb;
// $result=$deleted;



?>

<div class="content">
    <div class="result">
    <!-- <h3> Mapped results : <h3> -->
    
    <?php //vd_nicely($result); ?>
    <p>This script repairs the related entities from ODB to CADB</p>
    From $start to $limit.
    Operations are logged to log file. 

    </div>
</div>


<?php include('./inc/footer.inc');
?>

