<?php
// phpinfo();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require('./CAInterface.php');
// require('./CAInterface_example.php');
// require('./inc/functions.inc');

include('./inc/header.inc');
require('./config.php');
require('./db/CADB.php');
require('./db/ODB.php');


$cadb = new CADB();
$odb = new ODB();
/* connection to API */
// $ca_controller=new CAController('http://dev.bna-bbot.be/ctrl','thy','2GcWJxNAFMvJ64R','en_US', null);
// $ca_controller=new CAController('http://dev.bna-bbot.be/ctrl','god','openthegates','nl_BE', null);
$ca_controller=new CAController('http://dev.bna-bbot.be/ctrl','god','openthegates','fr_BE', null);
//locale : en_US, fr_BE
/* queries testing */

$track_odb=[];
// $tracks = $cadb->getCARecordingsNameSummary();
$tracks=$cadb->getCARecWithoutSummary();

// vd_nicely($tracks);

// foreach($tracks as $track){
//     echo('<h2>'.$track['object_id'].' / '.$track['name'].' : </h2>');
//     echo('<h3>'.$track['value_longtext1'].'</h3');
//     $found_odbtrack=$odb->getTrackIdBodyFromCATitle($track['name']);
//     $track_odb[]=$found_odbtrack;
//     vd_nicely($found_odbtrack);
//     //manage multiple results
// }



$summaries=[];

// $objectModel=json_decode(file_get_contents('../import/ca-models/recording_orig2.json'), True);

// $objectModel=array('attributes'=>
// array(
//     'summary' => 
//     array (
//       0 => 
//       array (
//         'locale' => 'fr_BE',
//         'summary' => 'test3',
//       ),
//     )
// ));



// vd_nicely($objectModel);
// $result=$ca_controller->updateRecording($objectModel,'ca_objects',562);
// $result=$ca_controller->getRecording('ca_objects',562);
// vd_nicely($result);
$count=0;
$start=19565;
$limit=23000;

$log  = 'REPAIRING SUMMARIES from ODB to CADB'.PHP_EOL.
        '===================================='.PHP_EOL.
        'This is a run from recordings id '.$start.' to '.($start+$limit).PHP_EOL;

file_put_contents('./log_summaries_'.date("j.n.Y").'.log', $log, FILE_APPEND);

foreach($tracks as $key=>$track){
    if($key>$start){
        echo('<br><br>id CA: ');
        echo($key);
        $track_odb=$odb->getTrackIdBodyFromCATitle($track['title'],$track['file'],'summary');
        if($track_odb){
            $count++;
            vd_nicely($track_odb);
        
            //update record in CA
            $objectModel=array('attributes'=>
            array(
                'summary' => 
                array (
                    0 => 
                    array (
                        'locale' => 'fr_BE',
                        'summary' => $track_odb[0]['Body'],
                    ),
                    )
                ));
            // uncomment here to update / leave comment to dry-run
            // $ca_controller->updateRecording($objectModel,'ca_objects',$key);
                
        //     //write to log
        
            $log  = date("F j, Y, g:i a").PHP_EOL.
            "Updating CA record - id: ".$key." / title: ".$track['title']." / file: ".$track['file'].PHP_EOL.
            "From ODB:".PHP_EOL.
            "   ID: ".$track_odb[0]['ID'].PHP_EOL.
            "   Title: ".$track_odb[0]['Title'].PHP_EOL.
            "   File: ".$track_odb[0]['File'].PHP_EOL.
            "   Type: ".$track_odb[0]['Type'].PHP_EOL.
            "   Body: ".$track_odb[0]['Body'].PHP_EOL.
            "-------------------------".PHP_EOL;
            //Save string to log, use FILE_APPEND to append.
            file_put_contents('./log_summaries_'.date("j.n.Y").'.log', $log, FILE_APPEND);
        }

    }   
    if($key>($start+$limit)) break;

}
$log="A total of ".$count." records have been processed.";
file_put_contents('./log_summaries_'.date("j.n.Y").'.log', $log, FILE_APPEND);
var_dump($count)
// var_dump(count($tracks));
// $result = $tracks;
// $result=$track_odb;
// $result=$deleted;



?>

<div class="content">
    <div class="result">
    <!-- <h3> Mapped results : <h3> -->
    
    <?php //vd_nicely($result); ?>
    <p>Repair the summaries from ODB to CADB</p>
    From $start to $limit.
    Operations are logged to log file. 

    </div>
</div>


<?php include('./inc/footer.inc');
?>

