<?php
define('__CA_SERVICE_API_USER__', 'administrator');
define('__CA_SERVICE_API_KEY__', 'dublincore');
define('_SERVICE_URL_', 'http://localhost');
define('__ODB_USER__', 'user');
define('__ODB_KEY__', 'password');
define('__LD_PATH__', __DIR__ . '/LanguageDetector/');
define('__LD_MODEL_FILE__', __DIR__ . '/import-utils/languages.php');
define('__LD_LOCALES_MAP__', array('french'=>'fr_BE', 'dutch' => 'nl_BE', 'english' => 'en_US'));
define('__ODB_CA_MAP_PATH__', __DIR__ . '/ODB-CA-id-map/');
define('__SOUND_FILES_IN_PATH__', '/home/path/to/sound_art/files');
define('__SOUND_FILES_OUT_PATH__', '/home/path/to/import/folder');
?>
