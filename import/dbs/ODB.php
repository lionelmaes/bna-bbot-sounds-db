<?php
class ODB{

  private $pdo = null;

  public function __construct(){

    if(!defined('__ODB_USER__') || !defined('__ODB_KEY__')){
      throw new \Exception('no user or password specified');
    }

    $options = [
    \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
    \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
    \PDO::ATTR_EMULATE_PREPARES   => false,
    ];

    $dsn = "mysql:host=localhost;dbname=backoffice;charset=latin1";

    try{
     $this->pdo = new \PDO($dsn, __ODB_USER__, __ODB_KEY__, $options);
    }catch (\PDOException $e) {
     throw new \PDOException($e->getMessage(), (int)$e->getCode());
    }
  }
  public function getOdbIDFromTitle($title){
    $stmt = $this->pdo->prepare(
      "SELECT ID
      FROM sound_arts
      WHERE Title=:title
      ");
    $stmt->execute(['title' => $title]);
    $data = $stmt->fetchAll();
    if(count($data) == 0)
      return false;
    return $data[0]['ID'];
  }
  public function getRecord($id){
    $stmt = $this->pdo->prepare(
      "SELECT *
      FROM sound_arts
      WHERE id=:id
      ");
    $stmt->execute(['id' => $id]);
    $data = $stmt->fetchAll();
    if(count($data) == 0)
      return false;
    return $data[0];

  }
  public function getAlbumFromAlbumId($id){
    $stmt = $this->pdo->prepare(
      "SELECT *
      FROM sound_arts
      WHERE AlbumID = :id
      AND Type = 'album'
      ");
    $stmt->execute(['id' => $id]);
    $data = $stmt->fetchAll();
    if(count($data) == 0)
      return false;
    return $data[0];

  }
  public function getAlbumFromDirName($dirname){
    //echo $dirname;
    $stmt = $this->pdo->prepare(
      "SELECT *
      FROM sound_arts
      WHERE Directory LIKE :dirname
      OR File LIKE :dirname2
      AND Type = 'album'
      ");
    $stmt->execute(['dirname' => $dirname.'%', 'dirname2' => $dirname.'%']);
    $data = $stmt->fetchAll();
    if(count($data) == 0)
      return false;
    return $data[0];


  }
  public function getContactsFromRecord($record){
    $contacts = array();
    //first get creator
    $stmt = $this->pdo->prepare(
      "SELECT *
      FROM contacts
      WHERE contacts.ID = :creatorID
      ");
    $stmt->execute(['creatorID' => $record['CreatorID']]);
    $data = $stmt->fetchAll();
    $contacts = array_merge($contacts, $data);
    //then the other relations
    $stmt = $this->pdo->prepare(
      "SELECT c.*, d.*
      FROM contacts as c
      JOIN dependencies as d ON d.TargetID = c.ID
      WHERE (d.DependencyTypeID = :babeleer
      OR d.DependencyTypeID = :kurieuzeneus)
      AND d.OriginID = :recordID
    ");

    $stmt->execute(['babeleer' => 33, 'kurieuzeneus' => 32, 'recordID' => $record['ID']]);
    $data = $stmt->fetchAll();
    $contacts = array_merge($contacts, $data);
    return $contacts;
  }

  public function getSoundsRecords($start = 0, $length = 10){
    $stmt = $this->pdo->prepare(
      "SELECT *
      FROM sound_arts
      LIMIT :start, :length
      ");
    $stmt->execute(['start' => $start, 'length' => $length]);
    $data = $stmt->fetchAll();

    foreach($data as &$record){
      $stmt = $this->pdo->prepare(
        "SELECT *
        FROM comments
        WHERE TargetID = :recordID AND ModuleTargetID = :moduleTargetID
        ");
        $stmt->execute(['recordID' => $record['ID'], 'moduleTargetID' => 16]);
        $record['comments'] = $stmt->fetchAll();
    }

    return $data;

  }

}
?>
