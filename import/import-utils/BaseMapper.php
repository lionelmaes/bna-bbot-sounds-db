<?php
//we'll need to detect languages for some texts from ODB
require(__LD_PATH__.'lib/LanguageDetector/autoload.php');

abstract class BaseMapper{
  private $ODBRecord = array();
  private $caModel = null;
  private $lastID = 0;


  public function __construct($lastID, $ODBRecord, $caModel){
    $this->LD = LanguageDetector\Detect::initByPath(__LD_MODEL_FILE__);
    $this->ODBRecord = $this->odbRecordPreProd($ODBRecord);

    $this->caModel = $caModel;
    $this->lastID = $lastID;

  }

  public function map(){
    //first, idno generation
    $idno = $this->buildIdno($this->ODBRecord, $this->lastID);

    $this->insertIntoModel($idno, 'intrinsic_fields,idno');
    //print_r($this->ODBRecord);
    //then, mapping!
    foreach($this->ODBRecord as $key => $value){

      if(isset($this->mappingTable[$key])){

        if(isset($this->mappingTable[$key][2]) && isset($this->mappingTable[$key][3])){
          $caValue = $this->convert($value, $this->mappingTable[$key][2], $this->mappingTable[$key][3]);

        }else{
          $caValue = html_entity_decode($value, ENT_QUOTES | ENT_HTML5);
        }
        if($caValue === false)
          continue;
        $this->insertIntoModel($caValue, $this->mappingTable[$key][0], $this->mappingTable[$key][1]);

      }

    }

  }
  private function convert($odbValue, $filter, $parameter){
    switch($filter){

      case 'textField':
        if(isset($parameter['emptyValue'])){
          if(
            is_array($parameter['emptyValue']) && in_array($odbValue, $parameter['emptyValue'])
            || $parameter['emptyValue'] === $odbValue
          )
          return false;
        }
        $body = html_entity_decode($odbValue, ENT_QUOTES | ENT_HTML5);

        if(isset($parameter['conversionTable'])){
          if(!isset($parameter['conversionTable'][$odbValue]))
            return false;
          $body = $parameter['conversionTable'][$odbValue];
        }

        $output = array($parameter['name'] => $body);
        if(isset($parameter['guessLocale']) && $parameter['guessLocale'] == true)
          $output['locale'] = $this->getLocale($body);
        if(isset($parameter['moreFields'])){
          foreach($parameter['moreFields'] as $key => $value){
            $output[$key] = $value;
          }
        }

        return $output;

      case 'textFields':
        $comments = array();
        foreach($parameter as $odbName => $caName){
          $comments[$caName] = array();
        }
        foreach($odbValue as $odbComment){
          $caComment = array();
          $body = html_entity_decode($odbComment['Body'], ENT_QUOTES | ENT_HTML5);
          $body = mb_convert_encoding($body, 'UTF-8', 'UTF-8');
          $caComment[$parameter[$odbComment['Type']]] = $body;
          $caComment['locale'] = $this->getLocale($body);

          $comments[$parameter[$odbComment['Type']]][] = $caComment;
        }
        return $comments;
    }

  }



  public function getCAModel(){
    return $this->caModel;
  }

  private function insertIntoModel($value, $path, $mode = 'set'){
    $path = explode(',', $path);

    $subArray = &$this->caModel;

    for($i = 0; $i < count($path); $i++){
      $ind = $path[$i];
      if($i < count($path) - 1)
        $subArray = &$subArray[$ind];
    }

    if($mode == 'set')
      $subArray[$ind] = $value;
    else if($mode == 'add')
      $subArray[$ind][] = $value;
    else if($mode == 'multipleset'){//set all the keys contained in $value (which is an array)
      foreach($value as $k => $v){
        $subArray[$ind][$k] = $v;
      }

    }

  }
  protected function validateDate($date, $format = 'Y-m-d'){
      $d = DateTime::createFromFormat($format, $date);
      // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
      return $d && $d->format($format) === $date;
  }
  protected function getLocale($str){
    try{
      $guess = $this->LD->detect($str);
    }
    catch (Exception $e) {
      return 'en_US';
    }
    if(is_array($guess))
      $lang = $guess[0]['lang'];
    else
      $lang = $guess;

    if(isset(__LD_LOCALES_MAP__[$lang]))
      return __LD_LOCALES_MAP__[$lang];
    else
      return 'en_US';
  }



  protected function filterDates($ODBRecord){
    if($ODBRecord['CreationDate'] == "0000-00-00 00:00:00"){
      if($ODBRecord['ModificationDate'] == "0000-00-00 00:00:00")
        $ODBRecord['ModificationDate'] = date('Y-m-d H:i:s');
      $ODBRecord['CreationDate'] = $ODBRecord['ModificationDate'];
    }
    else if($ODBRecord['ModificationDate'] == "0000-00-00 00:00:00"){
      $ODBRecord['ModificationDate'] = $ODBRecord['CreationDate'];
    }
    return $ODBRecord;
  }

}
?>
