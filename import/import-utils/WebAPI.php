<?php
  require_once('ca-service-wrapper/ItemService.php');
  require_once('ca-service-wrapper/BrowseService.php');
  require_once('ca-service-wrapper/SearchService.php');


  class WebAPI{
    private $browseClient = null;
    private $itemClient = null;
    private $types = array();
    public function __construct($types){
      $this->types = $types;
      $this->browseClient = new BrowseService(__SERVICE_URL__,"ca_objects","GET");
      $this->searchClient = new SearchService(__SERVICE_URL__,"ca_objects","GET");

      $this->itemClient = new ItemService(__SERVICE_URL__,"ca_objects","PUT");

    }

    public function insertRecording($model, $table){
      $this->itemClient->setTable($table);
      $this->itemClient->setRequestMethod('PUT');
      $this->itemClient->setRequestBody($model);

      $result = $this->itemClient->request();

      return($result->getRawData());

    }
    public function updateRecording($model, $table, $id){
      $this->itemClient->setTable($table);
      $this->itemClient->setRequestMethod('PUT');
      $this->itemClient->setRequestBody($model);
      $this->itemClient->addGetParameter("id", $id);
      $result = $this->itemClient->request();

      return($result->getRawData());
    }
    public function deleteRecording($table, $id){
      $this->itemClient->setTable($table);
      $this->itemClient->setRequestMethod('DELETE');
      $this->itemClient->addGetParameter("id", $id);
      $this->itemClient->setRequestBody(json_encode(array('hard'=>'true')));
      
      $result = $this->itemClient->request();
    }
    public function getRecording($table, $id){
        $this->itemClient->setTable($table);
        $this->itemClient->setRequestMethod('GET');
        $this->itemClient->addGetParameter("id", $id);
        $result = $this->itemClient->request();

        return($result->getRawData());
    }

    public function getObjectsWithoutMedia(){
      $this->browseClient->setRequestBody(
        array("criteria" =>
          array("has_media_facet"=> array(0)
          )
        )
      );



      $this->browseClient->setTable('ca_objects');
      $this->browseClient->addGetParameter("nocache", round(microtime(true) * 1000));

      $result = $this->browseClient->request();
      $resp = $result->getRawData();
      //print_r($resp);
      if(count($resp['results']) > 0)
        return($resp['results']);
      else return false;

    }
    /*
    public function getLastObjectInYear($table, $type, $year){
      $this->searchClient = new SearchService(__SERVICE_URL__, $table ,"GET");
      //$this->searchClient->setTable($table);

      $this->searchClient->addGetParameter("limit", 1);
      $this->searchClient->addGetParameter("sort", $table.".object_id");
      $this->searchClient->addGetParameter("sortDirection", "DESC");
      $this->searchClient->addGetParameter("noCache", 1);
      $this->searchClient->addGetParameter("no_cache", true);
      $this->searchClient->addGetParameter("dumb", round(microtime(true) * 1000));
      $this->searchClient->addGetParameter("q", $table.".idno:".round(microtime(true) * 1000)." OR ".$table.".idno:".$year);
      //echo $table.".idno:".$year." OR ".$table.".idno:".round(microtime(true) * 1000);
      $result = $this->searchClient->request();
      $resp = $result->getRawData();
      //print_r($resp);
      if(isset($resp['results']) && count($resp['results']) > 0)
        return($resp['results'][0]);

      else return false;

    }*/
    public function getLastRecord($table, $type){

      $this->browseClient->setRequestBody(
        array("criteria" =>
          array("type_facet"=> array($this->types[$type])
          )
        )
      );



      $this->browseClient->setTable($table);
      $this->browseClient->addGetParameter("limit", 1);
      $this->browseClient->addGetParameter("sort", $table.".id");
      $this->browseClient->addGetParameter("nocache", round(microtime(true) * 1000));
      $this->browseClient->addGetParameter("sortDirection", "DESC");
      //$this->browseClient->addGetParameter("include_deleted", 0);
      $result = $this->browseClient->request();
      $resp = $result->getRawData();
      //print_r($resp);
      if(count($resp['results']) > 0)
        return($resp['results'][0]);
      else return false;

    }

    public function getLastCARecordID($table, $type, $odbRecord){


      $lastCARecord = $this->getLastRecord($table, $type, $odbRecord);

      if($lastCARecord === false)
        $lastCARecordID = 0;
      else{
        $lastCARecordParts = explode('.', $lastCARecord['idno']);
        $lastCARecordID = $lastCARecordParts[count($lastCARecordParts)-1];
      }
      if(!preg_match("!^[0-9]+$!", $lastCARecordID))
        $lastCARecordID = 0;


      return $lastCARecordID;
    }

  }


?>
