<?php
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);
  require('config.php');
  require('dbs/ODB.php');
  require('dbs/CADB.php');
  require('import-utils/WebAPI.php');
  require('import-utils/ObjectMapper.php');
  require('import-utils/CollectionMapper.php');

  $json_params = file_get_contents("php://input");
  if (strlen($json_params) > 0)
    $p = json_decode($json_params, true);

  if(isset($p) && isset($p['action']) && $p['action'] == 'import'):
    $start = $p['start'];
    $limit = $p['limit'];
    $nostop = $p['nostop'];

    //echo json_encode(array('start' => $start, 'limit' => $limit, 'nostop' => $nostop));

    $output = array();


    $soundAlbumMap = json_decode(file_get_contents('./ODB-CA-id-map/sound-album.map.json'), True);
    $soundRecordingMap = json_decode(file_get_contents('./ODB-CA-id-map/sound-recording.map.json'), True);
    //$soundRecordingMap2 = json_decode(file_get_contents('./ODB-CA-id-map/sound-recording-pass2.map.json'), True);


    $webAPI = new WebAPI(array('recording'=>24, 'album' => 115));
    $odb = new ODB();
    $cadb = new CADB();
    $objectModel = json_decode(file_get_contents('./ca-models/recording_model.json'), True);
    $collectionModel = json_decode(file_get_contents('./ca-models/album_model.json'), True);

    $odbRecords = $odb->getSoundsRecords($start, $limit);

    foreach($odbRecords as $odbRecord){
      $outputLine = array();
      $outputLine['odbrecord'] = $odbRecord['ID'];
      if($odbRecord['Title'] == '') continue;

      if($odbRecord['Type'] == 'album'){
        if(isset($soundAlbumMap[$odbRecord['ID']]) && !isset($soundAlbumMap[$odbRecord['ID']]['errors'])){
          $outputLine['caAlbum'] = 'odb record '.$odbRecord['ID'].' already in sound-album mapping table';

        }
        else{
          $lastCAAlbumID = $webAPI->getLastCARecordID('ca_collections', 'album', $odbRecord);

          $cm = new CollectionMapper($lastCAAlbumID, $odbRecord, $collectionModel);
          $cm->map();


          //print_r($cm->getCAModel());
          $caOutput = $webAPI->insertRecording($cm->getCAModel(), 'ca_collections');
          //print_r($caID);
          //if(!isset($caOutput['errors']))
          $soundAlbumMap[$odbRecord['ID']] = $caOutput;
          $outputLine['caAlbum'] = $caOutput;

        }
      }
      if($odbRecord['Type'] == 'track' ||
         ($odbRecord['Type'] == 'album' && $odbRecord['File'] != '')
       ){
         if(isset($soundRecordingMap[$odbRecord['ID']]) && !isset($soundRecordingMap[$odbRecord['ID']]['errors'])){
           $outputLine['caRecording'] = 'odb record '.$odbRecord['ID'].' already in sound-recording mapping table';
         }
         else{
           $lastCARecordID = $cadb->getLastRecordingID($odbRecord);

          $outputLine['getlastID'] = $lastCARecordID;

           $om = new ObjectMapper($lastCARecordID, $odbRecord, $objectModel);
           $om->map();

           $outputLine['model'] = $om->getCAModel();

           $caOutput = $webAPI->insertRecording($om->getCAModel(), 'ca_objects');

           //if(!isset($caOutput['errors']))//we need to record the errors in the mapping file for further processing
           $soundRecordingMap[$odbRecord['ID']] = $caOutput;
           //$soundRecordingMap2[$odbRecord['ID']] = $caOutput;
           $outputLine['caRecording'] = $caOutput;

         }
       }

       $output[] = $outputLine;



    }
    //saving id mapping
    file_put_contents('./ODB-CA-id-map/sound-recording.map.json', json_encode($soundRecordingMap));
    //output map for second pass
    //file_put_contents('./ODB-CA-id-map/sound-recording-pass2.map.json', json_encode($soundRecordingMap2));
    file_put_contents('./ODB-CA-id-map/sound-album.map.json', json_encode($soundAlbumMap));
    echo json_encode($output);
    exit();
    //print_r($odb->getSoundsRecords());
  endif;
?>

<!doctype html>
<html>
  <head>
    <title>Import Objects</title>
    <link rel="stylesheet" href="ui/styles.css">
    <script src="ui/import.js" type="text/javascript"></script>
  </head>
  <body>
    <div class="log">
    </div>
    <form action="" method="post" id="form">

      <label for="start">start</label>
      <input type="number" name="start">
      <label for="limit">limit</label>
      <input type="number" name="limit">
      <label for="nostop">don't stop</label>
      <input type="checkbox" name="nostop">

      <input type="submit" name="action" value="start">
    </form>
  <body>
</html>
