<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once('config.php');
require_once('./ca-service-wrapper/ItemService.php');
require_once('./ca-service-wrapper/BrowseService.php');

/*GET THE LAST RECORD INSERTED*/

//$client = new BrowseService(__SERVICE_URL__,"ca_objects","OPTIONS");
/*
$client->setRequestBody(
  array("criteria" =>
    array("type_facet"=> array(24)
    )
  )
);

$client->addGetParameter("limit", 1);
$client->addGetParameter("sort", "ca_object_id");
$client->addGetParameter("sortDirection", "DESC");
*/
//$result = $client->request();

//print_r($result->getRawData());



/*INSERT A NEW OBJECT
$model = json_decode(file_get_contents('./ca-models/recording_model.json'), True);

//$model['intrinsic_fields']['idno'] = "OBJ.REC.2019.IN.2";
$model['preferred_labels'][0]['name'] ="test import object 2";
$model['attributes']['comment'][0]['comment'] = "test comment";

//print_r($model);

$client = new ItemService(__SERVICE_URL__,"ca_objects","PUT");

$client->setRequestBody($model);

$result = $client->request();

print_r($result->getRawData());
*/

#update an object
$model = json_decode(file_get_contents('./ca-models/test-up.json'), True);
$client = new ItemService(__SERVICE_URL__,"ca_objects","PUT");
$client->addGetParameter("id", 2);
$client->setRequestBody($model);
$result = $client->request();

print_r($result->getRawData());
?>
