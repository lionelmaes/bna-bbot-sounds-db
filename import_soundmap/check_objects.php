<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require('config.php');
require('dbs/ODB.php');
require('dbs/CADB.php');
require('import-utils/WebAPI.php');
require('import-utils/ObjectMapper.php');
require('import-utils/CollectionMapper.php');

$soundRecordingMap = json_decode(file_get_contents('./ODB-CA-id-map/sound-recording.map.json'), True);

function getOdbIDFromMap($caID, $soundRecordingMap){
  foreach($soundRecordingMap as $ODBID=>$caRef){

    if(isset($caRef['object_id']) && $caRef['object_id'] == $caID)
      return $ODBID;
  }
  return false;
}

function copyFile($path, $idno){
  $ext = pathinfo($path, PATHINFO_EXTENSION);
  $filename = pathinfo($path, PATHINFO_BASENAME);

  $outPath = __SOUND_FILES_OUT_PATH__.DIRECTORY_SEPARATOR.$idno;
  mkdir($outPath);
  copy($path, $outPath.DIRECTORY_SEPARATOR.$filename);
  return 'file '.$filename.' copied in'.$outPath;
}
//here we need to get all the objects withour media representation, find their corresponding odb record
//and check for file

$webAPI = new WebAPI(array('recording'=>24, 'album' => 115));
$odb = new ODB();
$caObjects = $webAPI->getObjectsWithoutMedia();
$emptyFileFieldCounter = 0;
$fileNotFoundCounter = 0;
$filesNotFound = [];
foreach($caObjects as $caObject){
  //print_r($caObject);
  $odbID = getOdbIDFromMap($caObject['object_id'], $soundRecordingMap);

  if($odbID === false){
    //echo "odb ID not found in mapping table. Trying to get it from title:\n";
    $odbID = $odb->getOdbIDFromTitle($caObject['display_label']);
    //echo $odbID."\n";
  }else{
    //echo "odb ID found from mapping table:\n";
    //echo $odbID."\n";
  }
  if($odbID === false){
    echo "error: no odb ID found!\n";
    continue;
  }
  $odbRecord = $odb->getRecord($odbID);
  $file = $odbRecord['File'];
  if($file == ""){
    echo "file field empty for this object ".$odbRecord['Title']."\n";
    $emptyFileFieldCounter++;
    continue;
  }

  //echo "file reference from odb: ".__SOUND_FILES_IN_PATH__.$file." \n";

  if(is_file(__SOUND_FILES_IN_PATH__.$file)){
    //ici on va tenter de checker si le fichier n'est pas déjà associé avec un enregistrement
    //copyFile(__SOUND_FILES_IN_PATH__.$file, $caObject['idno']);
    echo "en double? on delete ".$odbRecord['Title']."\n";
    $webAPI->deleteRecording('ca_objects', $caObject['object_id']);
  }else{
    $filesNotFound[] = __SOUND_FILES_IN_PATH__.$file;
    echo "file not found:".__SOUND_FILES_IN_PATH__.$file." for object ".$odbRecord['Title']."\n";
    $fileNotFoundCounter++;
  }


  //exit();
}

echo "empty file field: ".$emptyFileFieldCounter."\n";
echo "file not found: ".$fileNotFoundCounter."\n";
//echo json_encode($filesNotFound);
?>
