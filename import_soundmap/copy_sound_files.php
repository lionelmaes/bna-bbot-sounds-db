<?php
  require('config.php');
  require('officity-db-wrapper/ODB.php');
  require('import-utils/WebAPI.php');

  //first get odbRecords, then get filePath, then get idno from objects
  //then make dir with idno as dirname and copy file inside the new directory

  $soundRecordingMap = json_decode(file_get_contents('./ODB-CA-id-map/sound-recording.map.json'), True);
  //$soundRecordingMap = json_decode(file_get_contents('./ODB-CA-id-map/sound-recording-pass2.map.json'), True);
  function copyFile($path, $idno){
    $ext = pathinfo($path, PATHINFO_EXTENSION);
    $filename = pathinfo($path, PATHINFO_BASENAME);

    $outPath = __SOUND_FILES_OUT_PATH__.DIRECTORY_SEPARATOR.$idno;
    mkdir($outPath);
    copy($path, $outPath.DIRECTORY_SEPARATOR.$filename);
    return 'file '.$filename.' copied in'.$outPath;
  }


  $json_params = file_get_contents("php://input");
  if (strlen($json_params) > 0)
    $p = json_decode($json_params, true);

  if(isset($p) && isset($p['action']) && $p['action'] == 'import'):
    $start = $p['start'];
    $limit = $p['limit'];
    $nostop = $p['nostop'];

    //echo json_encode(array('start' => $start, 'limit' => $limit, 'nostop' => $nostop));

    $output = array();


    $webAPI = new WebAPI(array('recording'=>24, 'album' => 115));
    $odb = new ODB();

    $odbRecords = $odb->getSoundsRecords($start, $limit);

    foreach($odbRecords as $odbRecord){
    //foreach($soundRecordingMap as $id => $ref){
      //$odbRecord = $odb->getRecord($id);

      $outputLine = array();
      if($odbRecord['File'] == ''){
        $outputLine['message'] = 'No file for odb record '.$odbRecord['ID'].' > skipping';
        $output[] = $outputLine;
        continue;
      }
      $soundFile = __SOUND_FILES_IN_PATH__.$odbRecord['File'];
      if(!is_file($soundFile)){
        $outputLine['message'] = 'The file  '.$soundFile.' does not exist (?) > skipping';
        $output[] = $outputLine;
        continue;
      }
      if(!isset($soundRecordingMap[$odbRecord['ID']])){
        $outputLine['message'] = 'The ODB record  '.$odbRecord['ID'].' does not exist in sound-recording map';
        $output[] = $outputLine;
        continue;
      }
      $caEntry = $soundRecordingMap[$odbRecord['ID']];

      $caID = $caEntry['object_id'];
      $caObject = $webAPI->getRecording('ca_objects', $caID);

      //$outputLine['ca'] = $caObject;
      $idno = $caObject['idno']['value'];

      $outputLine['copy'] = copyFile($soundFile, $idno);

      $output[] = $outputLine;
    }





    echo json_encode($output);
    exit();
    //print_r($odb->getSoundsRecords());
  endif;
?>

<!doctype html>
<html>
  <head>
    <title>Copy sound files</title>
    <link rel="stylesheet" href="ui/styles.css">
    <script src="ui/import.js" type="text/javascript"></script>
  </head>
  <body>
    <div class="log">
    </div>
    <form action="" method="post" id="form">

      <label for="start">start</label>
      <input type="number" name="start">
      <label for="limit">limit</label>
      <input type="number" name="limit">
      <label for="nostop">don't stop</label>
      <input type="checkbox" name="nostop">

      <input type="submit" name="action" value="start">
    </form>
  <body>
</html>
