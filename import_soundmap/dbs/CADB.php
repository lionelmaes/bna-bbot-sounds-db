<?php
class CADB{
  private $pdo = null;

  public function __construct(){

    if(!defined('__CADB_USER__') || !defined('__CADB_KEY__')){
      throw new \Exception('no user or password specified');
    }

    $options = [
    \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
    \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
    \PDO::ATTR_EMULATE_PREPARES   => false,
    ];

    $dsn = "mysql:host=localhost;dbname=bnabbot_ca;charset=latin1";

    try{
     $this->pdo = new \PDO($dsn, __ODB_USER__, __ODB_KEY__, $options);
    }catch (\PDOException $e) {
     throw new \PDOException($e->getMessage(), (int)$e->getCode());
    }
  }


  public function getCARecordings(){
    $stmt = $this->pdo->prepare(
      "SELECT object_id, idno, idno_sort
      FROM ca_objects
      WHERE type_id = 24
      ORDER BY object_id
      ");
    $stmt->execute();

    $data = $stmt->fetchAll();

    return $data;

  }

  public function updateIdnoID($id, $idno, $idnosort){
    echo 'trying to set '.$idno.' <br />';
    try {
    $stmt = $this->pdo->prepare(
      "UPDATE ca_objects
      SET idno = :idno,
      idno_sort = :idnosort
      WHERE object_id = :id
      ");
    $stmt->execute(['idno' => $idno, 'idnosort' => $idnosort, 'id' => $id]);
    }catch (PDOException $e) {
    echo 'HUHOOOO: ' . $e->getMessage();
    }
  }

  public function getLastRecordingID($odbRecord){
    if($odbRecord['Date'] != "0000-00-00 00:00:00"){
      $year = substr($odbRecord['Date'], 0, 4);
    }else if($odbRecord['Date']!= "0000-00-00 00:00:00"){
      $year = substr($odbRecord['Date'], 0, 4);
    }
    if(!isset($year)) $year = date('Y');
    $stmt = $this->pdo->prepare(
      "SELECT *
      FROM ca_objects
      WHERE idno LIKE :search
      ORDER BY object_id
      DESC
      ");
    $stmt->execute(['search' => 'OBJ.REC.'.$year.'%']);
    $data = $stmt->fetchAll();
    if(count($data) == 0)
      return false;


    $lastCARecordParts = explode('.', $data[0]['idno']);
    $lastCARecordID = $lastCARecordParts[count($lastCARecordParts)-1];
    return $lastCARecordID;
  }
}

?>
