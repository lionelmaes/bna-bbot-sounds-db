<?php
  /* Regenerate all idnos recordings from table ca_objects */
  require('config.php');
  require('dbs/CADB.php');

  $cadb = new CADB();
  $caRecordings = $cadb->getCARecordings();

  $yearIndexes = array();

  foreach($caRecordings as $caRecording){
    $idnoParts = explode('.', $caRecording['idno']);
    $idnosortParts = explode('.', $caRecording['idno_sort']);

    $caRecordID = $idnoParts[count($idnoParts)-1];
    $caRecordYear = $idnoParts[count($idnoParts)-3];
    echo $caRecording['object_id'].' ';



    if(!isset($yearIndexes[$caRecordYear])){
      $newID = 1;
      $yearIndexes[$caRecordYear] = 1;

    }else{
      $yearIndexes[$caRecordYear] += 1;
      $newID = $yearIndexes[$caRecordYear];
    }
    if($newID != $caRecordID){
      $idnoParts[4] = $newID;
      $idnosortParts[4] = preg_replace('#[0-9]+#', $newID, $idnosortParts[4]);
      /*echo implode('.',$idnoParts);
      echo implode('.',$idnosortParts);*/
      $cadb->updateIdnoID($caRecording['object_id'], implode('.',$idnoParts), implode('.',$idnosortParts));
      echo $caRecordYear.' : update from '.$caRecordID.' to '.$newID.' <br>';
      //exit();
    }
    else
      echo $caRecording['idno'].' seems fine. Next one!<br>';


  }



?>
