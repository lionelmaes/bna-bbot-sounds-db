<?php

require_once('import-utils/BaseMapper.php');

class CollectionMapper extends BaseMapper{


  protected $mappingTable = array(
    'CreationDate' => array('attributes,date', 'add', 'textField', array(
      'name'=>'dates_value',
      'emptyValue' => '0000-00-00 00:00:00',
      'moreFields' => array('dc_dates_types' => 142)
    )),
    'ModificationDate' => array('attributes,date', 'add', 'textField', array(
      'name'=>'dates_value',
      'emptyValue' => '0000-00-00 00:00:00',
      'moreFields' => array('dc_dates_types' => 143)
    )),
    'Date' => array('attributes,date', 'add', 'textField', array(
      'name'=>'dates_value',
      'emptyValue' => '0000-00-00 00:00:00',
      'moreFields' => array('dc_dates_types' => 144)
    )),

    'Title' => array('preferred_labels,0,name', 'set'),

    'Languages' => array('attributes,lcsh_language', 'add', 'textField',
      array(
        'name' => 'lcsh_language',
        'conversionTable' => array(
          'fre' => 'French [info:lc\/vocabulary\/languages\/fre]',
          'bil' => 'Dutch [info:lc\/vocabulary\/languages\/dut]',
          'dut' => 'Dutch [info:lc\/vocabulary\/languages\/dut]',
          'eng' => 'English [info:lc/vocabulary/languages/eng]'
        )
      )
    ),
    'Context' => array(
      'attributes,recording_contexts','add','textField',
      array(
        'name' => 'recording_contexts',
        'conversionTable' => array(
          'spontaneous'=>146, 'suite'=>148, 'workshop'=> 147
        )
      )
    ),
    'ContextNote' => array(
      'attributes,recording_contexts_note','add','textField',
      array(
        'name' => 'recording_contexts_note',
        'emptyValue' => '',
        'guessLocale' => true
      )
    ),
    'Place' => array(
      'attributes,recording_location','add','textField',
      array(
        'name' => 'recording_location',
        'emptyValue' => '',
        'guessLocale' => true
      )
    ),

    'comments' => array('attributes', 'multipleset', 'textFields',
      array(
        'comment' => 'comment',
        'intention' => 'intention',
        'summary' => 'summary',
        'keyword' => 'tags'
      )
    )
  );

  protected function odbRecordPreProd($ODBRecord){
    $ODBRecord = $this->filterDates($ODBRecord);
    return $ODBRecord;

  }

  protected function buildIdno($ODBRecord, $lastID){
    
    return "COL.ALB.".($lastID + 1);


  }



}

?>
