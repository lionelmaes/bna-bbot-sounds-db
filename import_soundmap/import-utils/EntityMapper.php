<?php
/*[ID] => 1857
            [Activity] => 1
            [IsLocked] => 1
            [CreatorID] => 0
            [OwnerID] => 0
            [GroupID] => 0
            [ModifierID] => 1857
            [ContactType] => PP
            [Password] => 1be42b806a02c5ca
            [FirstName] => Boris
            [LastName] => Verdeyen
            [Denomination] =>
            [ClientCode] =>
            [Email1] => boris@nectil.com
            [EmailInvalid] => 0
            [Title] => Mr
            [Address] => 61 rue march&#233; au charbon
            [PostalCode] => 1000
            [City] => Bruxelles/Brussel
            [StateOrProvince] =>
            [CountryID] => bel
            [Nationality] => bel
            [Phone2] => 0485 49 75 40
            [MobilePhone] => 32.485.497.540
            [Fax] =>
            [LanguageID] => fre
            [Phone1] => 0
            [Email2] =>
            [WebSite] =>
            [Gender] => M
            [BirthDay] => 1979-05-09
            [PlaceOfBirth] =>
            [NationalNumber] =>
            [IDCardNumber] =>
            [SISNumber] =>
            [DigitalInfo1] =>
            [DigitalInfo2] =>
            [Vat] =>
            [RC] =>
            [Bank1] =>
            [Bank1IBAN] =>
            [Bank1Info] =>
            [Bank2] =>
            [Bank2IBAN] =>
            [Bank2Info] =>
            [Notes] => parce que pas !
            [AdminNotes] => test
            [CreationDate] => 2002-09-03 00:00:00
            [ModificationDate] => 2011-02-14 13:46:10
            [Privacy1] => 1
            [Privacy2] => 0
            [Purpose] => informaticien
            [BillingRate] => 0
            [Prefs] =>
            [Preview] =>
            [OriginCountryID] => 0
            [MaritalStatus] => 0
            [BXLInstallation] => 0000-00-00
            [ContactMe] => 1
            [SearchText] => boris verdeyen   61 rue marche au charbon 1000 bruxelles/brussel  bel informaticien 0 32.485.497.540  fre boris@nectil.com 0485 49 75 40 parce que pas !
*/
require_once('import-utils/BaseMapper.php');

class EntityMapper extends BaseMapper{


  protected $mappingTable = array(
    'FirstName' => array('preferred_labels,0,forename', 'set'),
    'LastName' => array('preferred_labels,0,surname', 'set'),
    'Title' => array('preferred_labels,0,prefix', 'set'),
    'Address' => array('attributes,address,0,address1', 'set'),
    'PostalCode' => array('attributes,address,0,postalcode', 'set'),
    'City' => array('attributes,address,0,city', 'set'),
    'CountryID' => array('attributes,address,0,country', 'set'),
    'Nationality' => array('attributes,lcsh_country', 'add', 'textField',
      array(
        'name'=>'lcsh_country',
        'conversionTable' => array(
          'bel' => 'Belgium [info:lc\/vocabulary\/countries\/be]',
          'col' => 'Colombia [info:lc\/vocabulary\/countries\/ck]',
          'fra' => 'France [info:lc\/vocabulary\/countries\/fr]',
          'esp' => 'Spain [info:lc\/vocabulary\/countries\/sp]',
          'ita' => 'Italy [info:lc\/vocabulary\/countries\/it]',
          'mar' => 'Morocco [info:lc\/vocabulary\/countries\/mr]',
          'cod' => 'Congo (Democratic Republic) [info:lc\/vocabulary\/countries\/cg]',
          'syr' => 'Syria [info:lc\/vocabulary\/countries\/sy]',
          'nld' => 'Netherlands [info:lc\/vocabulary\/countries\/ne]',
          'lux' => 'Luxembourg [info:lc\/vocabulary\/countries\/lu]',
          'grc' => 'Greece [info:lc\/vocabulary\/countries\/gr]',
          'tur' => 'Turkey [info:lc\/vocabulary\/countries\/tu]',
          'pol' => 'Poland [info:lc\/vocabulary\/countries\/pl]',
          'rwa' => 'Rwanda [info:lc\/vocabulary\/countries\/rw]',
          'ecu' => 'Ecuador [info:lc\/vocabulary\/countries\/ec]',
          'deu' => 'Germany [info:lc\/vocabulary\/countries\/gw]',
          'dji' => 'Djibouti [info:lc\/vocabulary\/countries\/ft]',
          'dza' => 'Algeria [info:lc\/vocabulary\/countries\/ae]',
          'rus' => 'Russia (Federation) [info:lc\/vocabulary\/countries\/ru]',
          'pak' => 'Pakistan [info:lc\/vocabulary\/countries\/pk]',
          'chl' => 'Chile [info:lc\/vocabulary\/countries\/cl]',
          'pan' => 'Panama [info:lc\/vocabulary\/countries\/pn]'
        )
      )
    ),
    'LanguageID' => array('attributes,lcsh_language', 'add', 'textField',
      array(
        'name' => 'lcsh_language',
        'conversionTable' => array(
          'fre' => 'French [info:lc\/vocabulary\/languages\/fre]',
          'bil' => 'Dutch [info:lc\/vocabulary\/languages\/dut]',
          'dut' => 'Dutch [info:lc\/vocabulary\/languages\/dut]',
          'eng' => 'English [info:lc/vocabulary/languages/eng]',
          'ara' => 'Arabic [info:lc/vocabulary/languages/ara]',
          'ger' => 'German [info:lc/vocabulary/languages/ger]',
          'spa' => 'Spanish [info:lc/vocabulary/languages/spa]',
          'ita' => 'Italian [info:lc/vocabulary/languages/ita]',
          'nor' => 'Norwegian [info:lc/vocabulary/languages/nor]',
        )
      )
    ),
    'Phone2' => array(
      'attributes,telephone', 'add', 'textField',
      array(
        'name' => 'telephone',
        'emptyValue' => 0
      )
    ),
    'Phone1' => array(
      'attributes,telephone', 'add', 'textField',
      array(
        'name' => 'telephone',
        'emptyValue' => 0
      )
    ),
    'MobilePhone' => array(
      'attributes,telephone', 'add', 'textField',
      array(
        'name' => 'telephone',
        'emptyValue' => 0
      )
    ),
    'Email1' => array(
      'attributes,email', 'add', 'textField',
      array(
        'name' => 'email',
        'emptyValue' => ''
      )
    ),
    'Email2' => array(
      'attributes,email', 'add', 'textField',
      array(
        'name' => 'email',
        'emptyValue' => ''
      )
    ),
    'Gender' => array('attributes,gender','add', 'textField',
      array(
        'name' => 'gender',
        'emptyValue' => ''
      )
    ),
    'BirthDay' => array('attributes,birthdate','add', 'textField',
      array(
      'name'=>'birthdate',
      'emptyValue' => '0000-00-00'
      )
    ),
    'BXLInstallation' => array('attributes,moveindate','add', 'textField',
      array(
      'name'=>'moveindate',
      'emptyValue' => '0000-00-00'
      )
    ),
    'Purpose' => array(
      'attributes,occupation','add','textField',
      array(
        'name' => 'occupation',
        'emptyValue' => ''
      )
    ),
    'Notes' => array(
      'attributes,biography','add','textField',
      array(
        'name' => 'biography',
        'emptyValue' => '',
        'guessLocale' => true
      )
    )


  );

  protected function odbRecordPreProd($ODBRecord){
    if(!$this->validateDate($ODBRecord['BXLInstallation']) || substr($ODBRecord['BXLInstallation'], 0, 4) == '0000'){
      $ODBRecord['BXLInstallation'] = '0000-00-00';
    }
    if(!$this->validateDate($ODBRecord['BirthDay']) || substr($ODBRecord['BirthDay'], 0, 4) == '0000'){
      $ODBRecord['BirthDay'] = '0000-00-00';
    }

    return $ODBRecord;

  }

  protected function buildIdno($ODBRecord, $lastID){
    return "ENT.IND.".($lastID + 1);
  }




}

?>
