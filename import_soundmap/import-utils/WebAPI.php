<?php
  require_once('ca-service-wrapper/ItemService.php');
  require_once('ca-service-wrapper/BrowseService.php');
  require_once('ca-service-wrapper/SearchService.php');


  class WebAPI{
    private $browseClient = null;
    private $itemClient = null;
    private $types = array();
    private $model = array();

    public function __construct($types, $model){
      $this->types = $types;
      $this->browseClient = new BrowseService(__SERVICE_URL__,"ca_objects","GET");
      $this->searchClient = new SearchService(__SERVICE_URL__,"ca_objects","GET");

      $this->itemClient = new ItemService(__SERVICE_URL__,"ca_objects","PUT");
      $this->model = $model;

    }

    public function insertRecording($model, $table){
      $this->itemClient->setTable($table);
      $this->itemClient->setRequestMethod('PUT');
      $this->itemClient->setRequestBody($model);

      $result = $this->itemClient->request();

      return($result->getRawData());

    }
    public function updateRecording($model, $table, $id){
      $this->itemClient->setTable($table);
      $this->itemClient->setRequestMethod('PUT');
      $this->itemClient->setRequestBody($model);
      $this->itemClient->addGetParameter("id", $id);
      $result = $this->itemClient->request();

      return($result->getRawData());
    }
    public function deleteRecording($table, $id){
      $this->itemClient->setTable($table);
      $this->itemClient->setRequestMethod('DELETE');
      $this->itemClient->addGetParameter("id", $id);
      $this->itemClient->setRequestBody(json_encode(array('hard'=>'true')));
      
      $result = $this->itemClient->request();
    }
    public function getRecording($table, $id){
        $this->itemClient->setTable($table);
        $this->itemClient->setRequestMethod('GET');
        $this->itemClient->addGetParameter("id", $id);
        $result = $this->itemClient->request();

        return($result->getRawData());
    }

    public function getObjectsWithoutMedia(){
      $this->browseClient->setRequestBody(
        array("criteria" =>
          array("has_media_facet"=> array(0)
          )
        )
      );



      $this->browseClient->setTable('ca_objects');
      $this->browseClient->addGetParameter("nocache", round(microtime(true) * 1000));

      $result = $this->browseClient->request();
      $resp = $result->getRawData();
      //print_r($resp);
      if(count($resp['results']) > 0)
        return($resp['results']);
      else return false;

    }
    /*
    public function getLastObjectInYear($table, $type, $year){
      $this->searchClient = new SearchService(__SERVICE_URL__, $table ,"GET");
      //$this->searchClient->setTable($table);

      $this->searchClient->addGetParameter("limit", 1);
      $this->searchClient->addGetParameter("sort", $table.".object_id");
      $this->searchClient->addGetParameter("sortDirection", "DESC");
      $this->searchClient->addGetParameter("noCache", 1);
      $this->searchClient->addGetParameter("no_cache", true);
      $this->searchClient->addGetParameter("dumb", round(microtime(true) * 1000));
      $this->searchClient->addGetParameter("q", $table.".idno:".round(microtime(true) * 1000)." OR ".$table.".idno:".$year);
      //echo $table.".idno:".$year." OR ".$table.".idno:".round(microtime(true) * 1000);
      $result = $this->searchClient->request();
      $resp = $result->getRawData();
      //print_r($resp);
      if(isset($resp['results']) && count($resp['results']) > 0)
        return($resp['results'][0]);

      else return false;

    }*/
    public function getLastRecord($table, $type){

      $this->browseClient->setRequestBody(
        array("criteria" =>
          array("type_facet"=> array($this->types[$type])
          )
        )
      );



      $this->browseClient->setTable($table);
      $this->browseClient->addGetParameter("limit", 1);
      $this->browseClient->addGetParameter("sort", $table.".id");
      $this->browseClient->addGetParameter("nocache", round(microtime(true) * 1000));
      $this->browseClient->addGetParameter("sortDirection", "DESC");
      // $this->browseClient->addGetParameter("include_deleted", 0);
      $result = $this->browseClient->request();
      $resp = $result->getRawData();
      // print_r($resp);
      if(count($resp['results']) > 0)
        return($resp['results'][0]);
      else return false;

    }

    public function getLastCARecordID($table, $type, $odbRecord){


      $lastCARecord = $this->getLastRecord($table, $type, $odbRecord);

      if($lastCARecord === false)
        $lastCARecordID = 0;
      else{
        $lastCARecordParts = explode('.', $lastCARecord['idno']);
        $lastCARecordID = $lastCARecordParts[count($lastCARecordParts)-1];
      }
      if(!preg_match("!^[0-9]+$!", $lastCARecordID))
        $lastCARecordID = 0;


      return $lastCARecordID;
    }

    public function getEntityDetail($id){
      $itemClient = $this->getItemClient('ca_entities', $id);
      $itemClient->setRequestBody(
        array("bundles" => array(
  
          "ca_entities.preferred_labels.displayname" => array(
  
          ),
          "ca_entities.preferred_labels.forename" => array(
          ),
          "ca_entities.preferred_labels.surname" => array(
          ),
          'ca_entities.lcsh_country' => array(),
          'ca_entities.biography' => array(),
          'ca_objects_x_entities.type_id' => array(
  
          ),
          'ca_objects.related.preferred_labels.name' => array(
          ),
          'ca_objects.related.idno' => array(
  
          )
        )
       )
      );
  
      $result = $itemClient->request();
      $rawData = $result->getRawData();
      // vd_nicely($rawData);
      $data = array(
        'name' => $rawData['ca_entities.preferred_labels.displayname'][0]['displayname'],
        'firstname' =>$rawData['ca_entities.preferred_labels.forename'][0]['forename'],
        'lastname' =>$rawData['ca_entities.preferred_labels.surname'][0]['surname'],
      );
      if(count($rawData['ca_entities.biography'][0]) > 0)
        $data['biography'] = $rawData['ca_entities.biography'][0]['biography'];
      $data['country'] = $rawData['ca_entities.lcsh_country'][0]['lcsh_country'];
      $data['objects'] = array();
      $i = 0;
      foreach($rawData['ca_objects.related.idno'] as $objectId){
        $data['objects'][$objectId] = array(
          'name' => $rawData['ca_objects.related.preferred_labels.name'][$i]['name'],
          'relation'=>$this->getRelationName($rawData['ca_objects_x_entities.type_id'][$i], 'ca_entities', 'ca_objects', 'individual', true),
  
          'ref'=> $objectId
        );
        $i++;
      }
      return $data;
    }

  public function entityNameExists($displayname){
    $table='ca_entities';
    $this->searchClient->setTable($table);
    $this->searchClient->addGetParameter("limit", 1);
    // $this->searchClient->addGetParameter("sort", $table.".object_id");
    // $this->searchClient->addGetParameter("sortDirection", "DESC");
    $this->searchClient->addGetParameter("noCache", 1);
    $this->searchClient->addGetParameter("no_cache", true);
    $this->searchClient->addGetParameter("q", $table.".preferred_labels.displayname:".$displayname);
    //echo $table.".idno:".$year." OR ".$table.".idno:".round(microtime(true) * 1000);
    $result = $this->searchClient->request();
    $resp = $result->getRawData();
    // print_r($resp);
    if(isset($resp['results']) && count($resp['results']) > 0)
      return($resp['results'][0]);

    else return false;
  }

//TODO TO FINISH
  public function insertEntity($odbRecord, $lastEntityID){
    $idno = $this->buildIdno($lastEntityID);
    $this->insertIntoModel($idno, 'intrinsic_fields,idno');
    //insertintomodel how?
    $displayname=$odbRecord['content']['firstname'].' '.$odbRecord['content']['lastname'];
    // print_r($displayname);
    $this->insertIntoModel($odbRecord['content']['firstname'],'preferred_labels,0,forename','set');
    $this->insertIntoModel($odbRecord['content']['lastname'],'preferred_labels,0,surname', 'set');
    $this->insertIntoModel($displayname,'preferred_labels,0,displayname', 'set');
    // print_r($this->model);
    $this->itemClient->setTable('ca_entities');
    $this->itemClient->setRequestMethod('PUT');
    $this->itemClient->setRequestBody($this->model);

    $result = $this->itemClient->request();

    return($result->getRawData());

  }
  public function insertSoundmapNode($odbRecord, $lastEntityID, $lastRecordID){
    //first, idno generation
    $idnoEntity = $this->buildIdno($lastEntityID);
    $idnoRecord = $this->buildIdno($lastRecordID);

    $this->insertIntoModel($idno, 'intrinsic_fields,idno');
    //print_r($this->ODBRecord);
    //then, mapping!
    foreach($odbRecord as $key => $value){
      print_r($key);

      switch($key){
        case 'id':
          print_r($value);
          break;
        case 'content':
          // foreach
          // cycle name => add related entity
          print_r($value);
          break;
        case 'sound_props':
          print_r($value);
          break;
        case 'date':
          print_r($value);
          break;
        case 'georeference':
          print_r($value);
          break;
        // case 'files':

      }  
      
      
        //insert into model
     
     }

     $this->itemClient->setTable('ca_entities');
     $this->itemClient->setRequestMethod('PUT');
     $this->itemClient->setRequestBody($this->model);

     $result = $this->itemClient->request();

     return($result->getRawData());

  }

  
  protected function buildIdno($lastID){
    return "ENT.IND.".($lastID + 1);
  }
  

  //TO CHECK 
  private function insertIntoModel($value, $path, $mode = 'set'){
    $path = explode(',', $path);

    $subArray = &$this->model;

    for($i = 0; $i < count($path); $i++){
      $ind = $path[$i];
      if($i < count($path) - 1)
        $subArray = &$subArray[$ind];
    }

    if($mode == 'set')
      $subArray[$ind] = $value;
    else if($mode == 'add')
      $subArray[$ind][] = $value;
    else if($mode == 'multipleset'){//set all the keys contained in $value (which is an array)
      foreach($value as $k => $v){
        $subArray[$ind][$k] = $v;
      }

    }

  }
  }


?>
