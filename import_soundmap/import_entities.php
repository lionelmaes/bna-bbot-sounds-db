<?php
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);
  require('config.php');
  require('dbs/ODB.php');
  require('import-utils/WebAPI.php');
  require('import-utils/EntityMapper.php');

  $json_params = file_get_contents("php://input");
  if (strlen($json_params) > 0)
    $p = json_decode($json_params, true);

  if(isset($p) && isset($p['action']) && $p['action'] == 'import'):
    $start = $p['start'];
    $limit = $p['limit'];
    $nostop = $p['nostop'];

    //echo json_encode(array('start' => $start, 'limit' => $limit, 'nostop' => $nostop));

    $output = array();


    $contactEntityMap = json_decode(file_get_contents('./ODB-CA-id-map/contact-entity.map.json'), True);

    $webAPI = new WebAPI(array('recording'=>24, 'album' => 115, 'individual' => 83));
    $odb = new ODB();
    $entityModel = json_decode(file_get_contents('./ca-models/entity_model.json'), True);

    $odbRecords = $odb->getSoundsRecords($start, $limit);

    foreach($odbRecords as $odbRecord){
      $outputLine = array();
      $outputLine['odbrecord'] = $odbRecord['ID'];
      $outputLine['contacts'] = array();

      //we need to find the contacts associated to the record
      $odbContacts = $odb->getContactsFromRecord($odbRecord);

      foreach($odbContacts as $odbContact){

        if(!isset($contactEntityMap[$odbContact['ID']])){
          //here we add the contact
          $lastCAEntityID = $webAPI->getLastCARecordID('ca_entities', 'individual', $odbContact);
          $em = new EntityMapper($lastCAEntityID, $odbContact, $entityModel);
          $em->map();


          $caOutput = $webAPI->insertRecording($em->getCAModel(), 'ca_entities');

          $contactEntityMap[$odbContact['ID']] = $caOutput;

          $outputLine['contacts'][] = array($em->getCAModel(), $caOutput);
        }
        else{
          $outputLine['contacts'][] = array('message' => 'entity '.$odbContact['ID'].' already in mapping table');
        }
      }
      $output[] = $outputLine;

    }

    //saving id mapping
    file_put_contents('./ODB-CA-id-map/contact-entity.map.json', json_encode($contactEntityMap));
    echo json_encode($output);
    exit();
    //print_r($odb->getSoundsRecords());
  endif;
?>

<!doctype html>
<html>
  <head>
    <title>Import Entities</title>
    <link rel="stylesheet" href="ui/styles.css">
    <script src="ui/import.js" type="text/javascript"></script>
  </head>
  <body>
    <div class="log">
    </div>
    <form action="" method="post" id="form">

      <label for="start">start</label>
      <input type="number" name="start">
      <label for="limit">limit</label>
      <input type="number" name="limit">
      <label for="nostop">don't stop</label>
      <input type="checkbox" name="nostop">

      <input type="submit" name="action" value="start">
    </form>
  <body>
</html>
