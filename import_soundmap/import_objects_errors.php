<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require('config.php');
require('dbs/ODB.php');
require('dbs/CADB.php');
require('import-utils/WebAPI.php');
require('import-utils/ObjectMapper.php');
require('import-utils/CollectionMapper.php');


$soundRecordingMap = json_decode(file_get_contents('./ODB-CA-id-map/sound-recording.map.json'), True);
$soundAlbumMap = json_decode(file_get_contents('./ODB-CA-id-map/sound-album.map.json'), True);

$webAPI = new WebAPI(array('recording'=>24, 'album' => 115));
$odb = new ODB();
$cadb = new CADB();

$objectModel = json_decode(file_get_contents('./ca-models/recording_model.json'), True);

foreach($soundRecordingMap as $odbID => $entry){
  if(isset($entry['errors'])){
    print_r($entry['errors']);
    $odbRecord = $odb->getRecord($odbID);
    //print_r($odbRecord);
    if($odbRecord['Title'] == '') continue;

    if($odbRecord['Type'] == 'album'){
      if(isset($soundAlbumMap[$odbRecord['ID']])){
        $outputLine['caAlbum'] = 'odb record '.$odbRecord['ID'].' already in sound-album mapping table';

      }
      else{
        $lastCAAlbumID = $webAPI->getLastCARecordID('ca_collections', 'album', $odbRecord);

        $cm = new CollectionMapper($lastCAAlbumID, $odbRecord, $collectionModel);
        $cm->map();


        print_r($cm->getCAModel());
        /*$caOutput = $webAPI->insertRecording($cm->getCAModel(), 'ca_collections');
        //print_r($caID);
        //if(!isset($caOutput['errors']))
        $soundAlbumMap[$odbRecord['ID']] = $caOutput;
        $outputLine['caAlbum'] = $caOutput;
        $oldCAAlbumID = $lastCAAlbumID;*/
      }
    }
    if($odbRecord['Type'] == 'track' ||
       ($odbRecord['Type'] == 'album' && $odbRecord['File'] != '')
     ){
        $lastCARecordID = $cadb->getLastRecordingID($odbRecord);


         $om = new ObjectMapper($lastCARecordID, $odbRecord, $objectModel);
         $om->map();
         print_r($om->getCAModel());
         //$caOutput = $webAPI->insertRecording($om->getCAModel(), 'ca_objects');

         //if(!isset($caOutput['errors']))//we need to record the errors in the mapping file for further processing
         $soundRecordingMap[$odbRecord['ID']] = $caOutput;
         //$outputLine['caRecording'] = $caOutput;


       }

  }
}




?>
