<?php
require('config.php');
require('dbs/ODB.php');
require('import-utils/WebAPI.php');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

function makeRelation(&$relationModel, $table, $relationType, $itemType, $id){
  switch($table){
    case 'ca_collections':
      $idFieldName = 'collection_id';
      break;
    case 'ca_entities':
      $idFieldName = 'entity_id';
    break;
  }
  $relationModel['related'][$table][] = array(
    $idFieldName => $id,
    'item_type_id'=> $itemType,
    'type_id' => $relationType
  );

}

$json_params = file_get_contents("php://input");
if (strlen($json_params) > 0)
  $p = json_decode($json_params, true);

if(isset($p) && isset($p['action']) && $p['action'] == 'import'):
  $start = $p['start'];
  $limit = $p['limit'];
  $nostop = $p['nostop'];

  $output = array();

  //mapping tables
  $soundRecordingMap = json_decode(file_get_contents('./ODB-CA-id-map/sound-recording.map.json'), True);
  $soundAlbumMap = json_decode(file_get_contents('./ODB-CA-id-map/sound-album.map.json'), True);
  $contactEntityMap = json_decode(file_get_contents('./ODB-CA-id-map/contact-entity.map.json'), True);

  $relationsIds = json_decode(file_get_contents('./ODB-CA-id-map/relations-id.json'), True);
  //the json model
  $relationModel = json_decode(file_get_contents('./ca-models/recording_relations_model.json'), True);
  $emptyRelationModel = $relationModel;

  $elementTypes = array('recording'=>24, 'album' => 115, 'individual' => 83);

  $relationTypes = array(
    'partof' => 102,
    'mainfile' => 103,
    'collectioncreator' => 112,
    'objectcreator' => 110,
    'objectinterviewee' => 107,
    'objectinterviewer' => 106
  );


  $webAPI = new WebAPI($elementTypes);
  $odb = new ODB();
  $odbRecords = $odb->getSoundsRecords($start, $limit);

  foreach($odbRecords as $odbRecord){
    $outputLine = array();
    $outputLine['odbrecord'] = $odbRecord['ID'];
    if(!isset($soundRecordingMap[$odbRecord['ID']])){
      $outputLine['message'] = 'odb record '.$odbRecord['ID'].' is not in sound recording mapping table';
      $output[] = $outputLine;
      continue;
    }
    if(isset($relationsIds[$odbRecord['ID']])){
      $outputLine['message'] = 'relations for odb record '.$odbRecord['ID'].' already set';
      $output[] = $outputLine;
      continue;
    }
    $caObject = $soundRecordingMap[$odbRecord['ID']];

    $caObjectID = $caObject['object_id'];
    $outputLine['carecord'] = $caObjectID;
    #region first we make relations between albums and recordings
    if($odbRecord['Type'] == 'album' && $odbRecord['File'] != ''){
      $caCollection = $soundAlbumMap[$odbRecord['ID']];
      $caCollectionID = $caCollection['collection_id'];
      //we must make a 'is main file' relation between $caObjectID and $caCollectionID
      makeRelation($relationModel, 'ca_collections', $relationTypes['mainfile'], $elementTypes['album'], $caCollectionID);


    }
    else if($odbRecord['Type'] == 'track'){
      //we need to find the album using the odb AlbumID field
      //or (if it's not set) using the directory name (the album should share the same dir)

      $odbAlbumTmpID = $odbRecord['AlbumID'];

      if($odbAlbumTmpID == 0 && $odbRecord['File'] != ''){
        $dirName = pathinfo($odbRecord['File'], PATHINFO_DIRNAME);
        $odbAlbum = $odb->getAlbumFromDirName($dirName);
        if($odbAlbum !== false){
          $odbAlbumID = $odbAlbum['ID'];
        }
      }
      else if($odbAlbumTmpID != 0){
        $odbAlbum = $odb->getAlbumFromAlbumId($odbAlbumTmpID);
        if($odbAlbum !== false){
          $odbAlbumID = $odbAlbum['ID'];
        }
      }


      if(isset($odbAlbumID) && isset($soundAlbumMap[$odbAlbumID])){
        $caCollection = $soundAlbumMap[$odbAlbumID];
        if(isset($caCollection['collection_id'])){
          $caCollectionID = $caCollection['collection_id'];
          //we must make a 'is partOf' relation between $caObjectID and $caCollectionID
          makeRelation($relationModel, 'ca_collections', $relationTypes['partof'], $elementTypes['album'], $caCollectionID);
        }
        else{
          $outputLine['message'] = 'couldn\'t find collection_id for in soundAlbumMAp at odbAlbumID '.$odbAlbumID;
        }
      } 
      else if(!isset($odbAlbumID)){
        $outputLine['message'] = 'couldn\'t find odbAlbumID for track';
      }else{
        $outputLine['message'] = 'couldn\'t find reference in soundAlbumMap for $odbAlbumID '.$odbAlbumID;
      }
    }
    #endregion
    #region then we make relations between entities and recordings
    $contacts = $odb->getContactsFromRecord($odbRecord);
    $caContacts = array();
    foreach($contacts as $contact){
      if(!isset($contactEntityMap[$contact['ID']]) || isset($contactEntityMap[$contact['ID']]['errors'])){
        continue;
      }
      $caEntity = $contactEntityMap[$contact['ID']];
      $caContacts[] = $caEntity;

      $caEntityID = $caEntity['entity_id'];
      if(!isset($contact['DependencyTypeID'])){//creator
        $relationType = 'objectcreator';
      }
      else if($contact['DependencyTypeID'] == 33){//interviewee
        $relationType = 'objectinterviewee';
      }
      else if($contact['DependencyTypeID'] == 32){//interviewer
        $relationType = 'objectinterviewer';
      }
      if(isset($relationType))
        makeRelation($relationModel, 'ca_entities', $relationTypes[$relationType], $elementTypes['individual'], $caEntityID);



    }
    #endregion
    $outputLine['relations'] = $relationModel;
    $outputLine['ca entities'] = $caContacts;
    $outputLine['result'] = $webAPI->updateRecording($relationModel, 'ca_objects', $caObjectID);
    $relationsIds[$odbRecord['ID']] = $outputLine['result'];
    $relationModel = $emptyRelationModel;
    $output[] = $outputLine;
  }
  file_put_contents('./ODB-CA-id-map/relations-id.json', json_encode($relationsIds));
  echo json_encode($output);
  exit();
endif;
?>

<!doctype html>
<html>
  <head>
    <title>Import Relations</title>
    <link rel="stylesheet" href="ui/styles.css">
    <script src="ui/import.js" type="text/javascript"></script>
  </head>
  <body>
    <div class="log">
    </div>
    <form action="" method="post" id="form">

      <label for="start">start</label>
      <input type="number" name="start">
      <label for="limit">limit</label>
      <input type="number" name="limit">
      <label for="nostop">don't stop</label>
      <input type="checkbox" name="nostop">

      <input type="submit" name="action" value="start">
    </form>
  <body>
</html>
