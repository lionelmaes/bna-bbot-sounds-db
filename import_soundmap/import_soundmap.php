<?php
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);
  require('config.php');
  require('dbs/ODB.php');
  require('dbs/CADB.php');
  require('import-utils/WebAPI.php');
  require('import-utils/ObjectMapper.php');
  require('import-utils/CollectionMapper.php');
  // require('../db_interface/CAInterface.php');

//var_dump prettier
function vd_nicely($data){
  return highlight_string("\n\n<?php\n" . var_export($data, true) . ";\n?>");
}

  $json_params = file_get_contents("php://input");
  if (strlen($json_params) > 0)
    $p = json_decode($json_params, true);

  if(isset($p) && isset($p['action']) && $p['action'] == 'import'):
    // echo('heillo');
    $start = $p['start'];
    $limit = $p['limit'];
    $nostop = $p['nostop'];

  
    $output = array();
    $entityModel = json_decode(file_get_contents('./ca-models/entity_model.json'), True);
    $recordModel = json_decode(file_get_contents('./ca-models/recording_model.json'), True);
    $webAPIEntity = new WebAPI(array('recording'=>24, 'album' => 115, 'individual' => 83), $entityModel);
    $webAPIRecord = new WebAPI(array('recording'=>24, 'album' => 115, 'individual' => 83), $recordModel);
    
    $odb = new ODB();

    // $cadb = new CADB();


    $odbRecords = $odb->getSoundMapNodes($start, $limit);
    $lastCAEntityID = $webAPIEntity->getLastCARecordID('ca_entities', 'individual', null);
    $lastCARecordID = $webAPIRecord->getLastCARecordID('ca_objects', 'recording', null);

    foreach($odbRecords as $odbRecord){

      
      $outputLine = array();
      $outputLine['id'] = $odbRecord['id'];

  
          // $caOutput = $webAPI->insertRecording($cm->getCAModel(), 'ca_collections');
          
          
          // if not exist
          if (isset($odbRecord['content']['firstname']) & isset($odbRecord['content']['lastname'])){
            $displayname=$odbRecord['content']['firstname'].' '.$odbRecord['content']['lastname'];
            $outputLine['displayname']=$displayname;
            if ($webAPIEntity->entityNameExists($displayname)){
              $outputLine['already_exists']='true';
              
              
            }else{
              $outputLine['import_status']=$webAPIEntity->insertEntity($odbRecord,$lastCAEntityID);
              //je n'ajoute pas le email car c'est souvent l'email de qq de bna bott !!!
              
            }
            // $caOutput = $webAPI->insertSoundmapNode($odbRecord, $lastCAEntityID, $lastCARecordID);
            // insert into json file IDNO / sound or img / path => to download later all the paths => to upload later to Collective Access with structured folders
            
          }
        





          $output[] = $outputLine;



 
    // echo json_encode($output);

    }
    
    // $output['lastid']=$lastCAEntityID;
    echo json_encode($output);
    // echo json_encode('hello'); 
    exit();
  endif;


  $odb = new ODB();
  // $odbRecords = $odb->getSoundMapNodes(109, 1);
  $odbRecords = $odb->getSoundMapNodes(109, 10);
  vd_nicely($odbRecords);
  // $outputLine=array();
//   foreach($odbRecords as $odbRecord){
//   if (isset($odbRecord['content']['firstname']) && isset($odbRecord['content']['lastname'])){
//     $displayname=$odbRecord['content']['firstname'].' '.$odbRecord['content']['lastname'];

//       $outputLine['displayname']=$displayname;  
//       print_r($outputLine);

//   }
  
// }

?>

<!doctype html>
<html>
  <head>
    <title>Import Objects</title>
    <link rel="stylesheet" href="ui/styles.css">
    <script src="ui/import.js" type="text/javascript"></script>
    <meta charset="UTF-8">
  </head>
  <body>
    <div class="log">
    </div>
    <form action="" method="post" id="form">

      <label for="start">start</label>
      <input type="number" name="start">
      <label for="limit">limit</label>
      <input type="number" name="limit">
      <label for="nostop">don't stop</label>
      <input type="checkbox" name="nostop">

      <input type="submit" name="action" value="start">
    </form>
  <body>
</html>
