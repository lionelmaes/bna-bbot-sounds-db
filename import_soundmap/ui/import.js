const controller = new AbortController()
const signal = controller.signal;
var errors = 0;
function submitForm(e, form, log){
  e.preventDefault();
  if(form.action.value == 'start'){
    form.action.value='stop';
    fetch(location.href.split("/").slice(-1), {
      method: 'post',
      signal: signal,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        action: 'import',
        start: form.start.value,
        limit: form.limit.value,
        nostop:form.nostop.checked
      })
    }).then(function(response) {
      return response.json();
    }).then(function(data) {
      if(data.length == 0)
        return;
      
      logData(data, log);
      if(form.nostop.checked){
        form.start.value = Number(form.start.value) + Number(form.limit.value);
        form.action.value='start';
        submitForm(e, form, log);
      }else{
        form.action.value='start';
      }
    }).catch(function(err) {
      //Failure
      form.action.value='start';
      logError(err, log);
      errors++;
      if(form.nostop.checked && errors < 5){
        submitForm(e, form, log);
      }
    });
  }else{
    form.action.value='start';
    controller.abort();

  }
}
function logData(data, log){
  log.style.display = 'block';

  for(var i in data){
    var line = document.createElement('div');
    line.className = "line";
    line.innerHTML = JSON.stringify(data[i]);
    log.appendChild(line);
    window.scrollTo(0,document.body.scrollHeight);
  }
}
function logError(err, log){
  log.style.display = 'block';
  var line = document.createElement('div');
  line.className = "line error";
  line.innerHTML = err;
  log.appendChild(line);
  window.scrollTo(0,document.body.scrollHeight);
}
document.addEventListener("DOMContentLoaded", function() {
  var form = document.querySelector('#form');
  var log = document.querySelector('.log');

  form.addEventListener('submit', function(evt){

    submitForm(evt, this, log);
  });
});
